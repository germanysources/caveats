# Stolperfallen bei der Arbeit mit der Stripe-API (https://stripe.com/docs/api)

## Abos (Subscriptions)
### Gutscheine
- Ist der Gutscheinbetrag >= Abobetrag (ein Interval), wird keine Rechnung erstellt (`latest_invoice` ist leer). 

### Billing Cycle Anchor 
- Dieser darf höchstens ein Interval in der Zukunft liegen. Bei Monatsabos darf Billing Cycle Anchor höchstens einen Monat in der Zukunft liegen, bei Jahresabos höchstens ein Jahr.
- Bei `proration_behavior: none` wird für den Zeitraum Erstellung Abo bis Billing Cycle Anchor keine Rechnung erstellt und die Zahlungsmethode nicht belastet.

## Rechnungen
- Rechnungen lassen sich nicht mit einem Preis, der das `recurring`-Attribut besitzt, manuell erstellen. Die Preise mit dem `recurring`-Attribut sind nur für Abos gedacht.
