# Stolperfallen bei der Arbeit mit SAP BW

## Versehentliches Löschen von Delta-Queues
Um die Delta-Queues wieder herzustellen, muss in den InfoPackages eine erneute Initialisierung durchgeführt werden. Dazu ist in der Transaktion RSA1 im Reiter `Fortschreibung` `Initialisierung des Delta-Verfahrens` -> `Initialisierung ohne Datenübertragung` zu wählen und im Reiter `Einplanung` der Job zu starten.
![RSA1 Info Package](info_package1.png)

## Erst Init-Request ... löschen vor nochmaligem Init mit überlappender Sel (Meldung Nachrichtenklasse RSM Nummer 1188).
Init-Requests können wie im SAP-Hinweis [2964474](https://me.sap.com/notes/0002964474) beschrieben gelöscht werden. Tritt diese Meldung beim Laden der Prozesskette auf, ist im InfoPackage noch Initialisierung als Fortschreibungsmethode gewählt anstatt des Delta- bzw. Full-Uploads.

## Extraktionsstrukturen
Änderungen an den Extraktionsstrukturen können in den Reports des Logistik-Informations-Systems (LIS) (Reportnamen RMBWV3xx) zu folgenden Laufzeitfehlern führen.
```
Category ABAP Programming Error
Runtime Errors MESSAGE_TYPE_X
ABAP Program SAPLMCEX
Application Component LO-LIS
(...)
| Message class....... "MCEX"
| Number.............. 194
(...)
"MESSAGE_TYPE_X"
"SAPLMCEX" bzw. LMCEXUnn
"MCEX_UPDATE_nn"
(...)
>>>>> message x194(mcex) with sy-subrc.
```
Strukturänderungen werden unter anderem auch durch Unicode-Umstellung hevorgerufen. Lösungen sind im Hinweis [2387875](https://me.sap.com/notes/2387875/) beschrieben.
