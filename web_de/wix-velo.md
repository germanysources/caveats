# Umgebung
- Wix Velo Release Mar. 2023

# Wix-Velo

## Wix-Data
Die query-Funktion kann wie im folgenden Listing nicht einzeln importiert werden. Dies hat im Frontend-Code den Fehler `this is undefined` zur Folge und im Backend den Fehler `provider is undefined`:
```js
import {query} from 'wix-data';

export async function readBlogPosts(){
  const {items: posts} = await query('Blog/Posts').find();
  return posts;
}
```
Stattdessen muss das gesamte Modul importiert werden.
```
import wixData from 'wix-data';

export async function readBlogPosts(){
  const {items: posts} = await wixData.query('Blog/Posts').find();
  return posts;
}
```

## Datasets
- Felder, die persönliche Infos enthalten, lassen sich nicht filtern.
- Submit: Nach dem Submit werden alle Felder initialisiert. Wird im `onAfterSave`-Handler auf die Felder mit dem `$w`-Selektor zugegriffen, sind die Werte undefiniert.
```js
$w('#dataset1').onAfterSave(() => {
  console.log($w('#address').value); // -> gibt undefined aus
});
```
- Setzen eines Wertes in der `value`-Eigenschaft hat keine Wirkung für Eingabefelder mit Datenbindung zu einem Dataset. D. h. beim Submit wird ein leeres Feld übermittelt.
```js
$w('#name').value = 'Wix Velo';
```

- Vorbelegen Felder: Wird der Funktion `setFieldValues` ein `_id` mitgegeben, hat dies beim Submit ein Update zur Folge.

### Multi-Referenzen
Datasets enthalten keine Multi-Referenzen. Von einem Repeater, der mit einem Dataset verbunden ist, bekommt man keine Multi-Referenzen in der `forEachItem`-Methode wie im folgenden Listing:
```js
$w('#dataset1').onReady(() => {
  $w('#repeater1').forEachItem(($item, itemData, index) => {
    // multiReferenceField sei das Multi-Referenz-Feld
    console.log(itemData.multiReferenceField); // -> gibt undefined aus
  });
});
```
Beim Sichern werden die Multi-Referenzen auch nicht in die Datenbank eingefügt.

#### `isReferenced`
Multireferenzen auf eine Wix-App-Kollektion liefern den Fehler `WDE0149: {"message":"internal_error, details: {\"error\":\"No value found for '_owner'\"}","details":{"category":"internal_error","error":"No value found for '_owner'"}}`, wenn die Funktion nicht als Admin gerufen wird. Wix-App-Kollektionen haben kein `_owner`-Attribute. Im folgenden Listing hat die Kollektion `Favorites` eine Multireferenz zu der Kollektion `Members/PrivateMemberData`.
```js
import wixData from 'wix-data';
import {currentMember} from 'wix-members-backend';

async function isReferenced(id){
  // wirft Fehler WDE0149
  return await wixData.isReferenced('Favorites', 'member', id, (await currentMember.getMember)['_id']);
}
```

#### `queryReferenced`
Multireferenzen auf eine Wix-App-Kollektion liefern den Fehler `WDE0076: Validation error. Invalid filter operation $eq for field '_owner'.`, wenn die Funktion nicht als Admin gerufen wrd. Wix-App-Kollektionen haben kein `_owner`-Attribute. Im folgenden Listing hat die Kollektion `Favorites` eine Multireferenz zu der Kollektion `Members/PrivateMemberData`.
```js
import wixData from 'wix-data';
import {currentMember} from 'wix-members-backend';

async function queryReferenced(id){
  // wirft Fehler WDE0076
  return await wixData.queryReferenced('Favorites', id, 'member');
}
```

## Datenbindung mit Rich-Text-Elementen
Gestaltung des Textfeldes (Schriftart, Farbe, Zentrierung) wird für Textfelder mit Datenbindung an Rich-Text-Elemente ignoriert, wenn das HTML-Attribut komplett wie im folgenden Listing überschrieben wird.
```js
$w('#text1').html = `<span style="font-weight: bold">Hallo Welt</span>`
```
Die bestehende DOM-Knoten müssen erhalten bleiben, damit die Gestaltung erhalten bleibt. Das Element `#text1` erhält als initialer Text `Hallo Welt`, der nur ersetzt wird.
```js
$w('#text1').html = $w('#text1').html.replace('Hallo Welt', '<span style="font-weight: bold">Hallo Welt</span>')
```

## Kollektion von Wix-Apps
Nicht alle Felder von vordefinierten Kollektion der Wix-Apps (Store, Blog ...) lassen sich beliebig filtern. Siehe:
- [Store/Orders](https://support.wix.com/en/article/velo-wix-stores-orders-collection-fields)
- [Store/Products](https://support.wix.com/en/article/velo-wix-stores-products-collection-fields)
- [Blog/Posts](https://support.wix.com/en/article/velo-wix-blog-posts-collection-fields)
- [Blog/Categories](https://support.wix.com/en/article/velo-wix-blog-categories-collection-fields)

## Tabellen
Die Selektion bleibt bestehen. Ein mehrfaches Selektieren derselben Zeile hat nur einen Aufruf des Event-Handlers zur Folge.

## Adressfelder
```js
console.log($w('#address').value);
```
Wert kann nur `formatted` enthalten, wenn die Eingabe aus der Autovervollständigung (Browser) oder aus der Zwischenablage (Clipboard) stammt. Die Eigenschaften `streetAddress`, `city`, `country` usw. fehlen in diesem Fall.

## Repeater
Event-Handler, die im `onItemReady` bzw. `forEachItem`-Handler an Eingabeelemente gebunden werden, werden:
- nach dem Laden der Seite einmal aufgerufen,
- nach der 1.Aktualisierung des Repeaters zweimal
- nach der 2.Aktualisierung des Repeaters dreimal
usw.

Um dies zu vermeiden, sollten die Event-Handler ausserhalb der `onItemReady` bzw. `forEachItem`-Handler an Eingabeelemente gebunden werden (siehe [Velo-Forum](https://www.wix.com/velo/forum/coding-with-velo/onclick-triggering-twice)) wie im folgenden Listing.
```js
$w('#repeater1').onItemReady(($item, itemData, index) => {
  $item('#airline').text = itemData.airline;
});
$('#airline').onClick(event => {
  const $item = $w.at(event.context);
  console.log(`{$item('#airline').text} selected`);
});
```

Beim Festlegen von Repeaterdaten muss eine eindeutige ID vergeben werden. Diese darf nur alphanumerische Zeichen (A-Z,a-z,0-9) und Bindestriche enthalten. Wird die ID über eine externe Bibliothek generiert, muss diese Restriktion beachtet werden. Die Verwendung von [nanoid](https://npmjs.com/package/nanoid) ist beispielsweise nicht möglich, da die ID auch Unterstriche enthalten kann.
```js
import {nanoid} from 'nanoid';

$w.onReady(() => {
  // Fehlerträchtig wegen Verwendung von nanoid
  $w('#repeater1').data = [
   {
     _id: nanoid(),
     text: 'Walking'
   },
   {
     _id: nanoid(),
     text: 'Bicycle'
   }
  ];
});
```

## Switch-Elemente
```js
$w('#switchElement').onChange(event => console.log(event.target.value));
```
Ist der Switch ausgeschaltet, wird derselbe Wert ausgegeben als wenn der Switch eingeschaltet ist.

## Tabs
In EditorX kommt der Fehler `Couldn't load component SDK`, wenn auf Tabs zugegriffen wird:
```js
console.log($w('#tabs1').tabs);
```

## TextInput
Im onKeyPress-Ereignisbehandler besitzt das Textfeld noch den Wert, denn es vor dem Tastendruck hatte.
```js
$w('#textInput1').onKeyPress(event => {
  console.log(event.target.value);
});
```

## iOS Safari (Version 15.4.1)
### Dropdown-Felder, die mit einem Dataset verbunden sind
Im `onChange`-Event-Handler erhält `event.target.value` teilweise noch den vorherigen Wert, nicht den aktuellen. Dies trifft zu, wenn im Browser vor und zurück navigiert wird.
### Frontend-Code
Beim Zurücknavigieren werden die Event-Handler nicht erneut ausgeführt (todo Untersuchen).

## Streams
Streams lassen sich nicht in einer Kollektion sichern. Bsp.:
```js
import {get} from 'axios';
import wixData from 'wix-data';

const response = await get('http://localhost/binary', {responseType: 'stream'});
await wixData.insert('Collection', {object: {stream: response.data}});
```
Hier kommt es zur `Maximum call stack size exceeded`-Ausnahme.

## Web-Worker
Frontend-Code läuft im Web-Worker-Thread. Sobald der Tab geschlossen wird bzw. in eine andere App navigiert wird (Mobile Geräte), beendet sich der [Web-Worker](https://stackoverflow.com/questions/8875310/what-happens-to-an-html5-web-worker-thread-when-the-tab-is-closed-while-its-run?noredirect=1&lq=1).

## WDE0053 Unknown Error (wix-data-modul)
Ursachen können sein:
- [60 Sek. Token](https://www.wix.com/velo/forum/coding-with-velo/unknown-error-inserting-to-collection-after-1-minute)
Es können Retries mit einem Timeout implementiert werden als Workaround. 

## Storage
Größe ist limitiert:
- 50KB für Local und Session Storage
- 1MB für Memory Storage

## `wix-pricing-plans`
Ist kein Mitglied eingeloggt und die aktuellen Abos werden abgefragt, erscheint eine leere Seite:
```js
import { orders } from 'wix-pricing-plans';

$w.onReady(() => {
  orders.listCurrentMemberOrders().then(console.log).catch(console.error);
});
```

Beim Erstellen eines Abos ist die Dokumentation irreführend. Options ist kein Objekt, Das Startdatum muss einzeln angegeben werden.
Negativbeispiel:
```js
import {checkout} from 'wix-pricing-plans';

$w.onReady(function(){
  $w('#purchaseButton').onClick(async () => {
    await checkout.startOnlinePurchase(pricingPlans[0].pricingPlan, {startDate: $w('#subscriptionStartDate'}).value);
  });
});
```

Positivbeispiel:
```js
import {checkout} from 'wix-pricing-plans';

$w.onReady(function(){
  $w('#purchaseButton').onClick(async () => {
    await checkout.startOnlinePurchase(pricingPlans[0].pricingPlan, $w('#subscriptionStartDate').value);
  });
});
```

Das Startdatum kann nur bei Plänen angegeben werden, bei denen die Einstellung "Let people set start date" gesetzt ist. Bei kostenlosen Plänen ist diese Einstellung immer false und kann nicht gesetzt werden.

## `wix-pricing-plans-backend`
Eine sofortige Stornierung eines Plans mit anstehender Stornierung (`autoRenewCanceled=true`) ist mit der Funktion `cancelOrder` nicht möglich.
```js
import {orders} from 'wix-pricing-plans-backend';

export async function cancel(orderId){
  await cancelOrder(orderId, 'IMMEDIATELY'); // -> wirft Fehler
}
```

Ein mehrfaches Ordern desselben Plans mit der Funktion `checkout.createOfflineOrder` hat unter Umständen folgenden Fehler zur Konsequenz, auch wenn der Plan mehrfaches Ordern erlaubt:
```
details:
  applicationError:
    description: Unauthorized
    code: UNAUTHORIZED
    data: {}
```

## `wix-secrets-backend`
Seitenmitglieder haben keine Berechtigung Secrets zu erstellen.

Entgegen der Dokumentation muss dieses Modul seit dem 28.02.23 wie folgt importiert werden:
```js
import * as wixSecretsBackend from 'wix-secrets-backend';
```

## `wix-media-backend`
Wird kein Node.js-Buffer-Objekt der `upload`-Methode übergeben, führt diese zum Fehler `TypeError: source.on is not a function`
```js
import {mediaManager} from 'wix-media-backend';

export async function uploadFile(){
  const file = [4, 5, 1];
  return await mediaManager.uploadFile('folder', file, {mediaOptions: {mediaType: 'document'}});
}
```

## `wix-members`
Das aktuelle Mitglied wird mit Fieldset-Option `FULL` ohne vollständige Kontaktdaten zurückgegeben.
```js
import {currentMember} from 'wix-members';

$w.onReady(() => {
  console.log(await currentMember.getMember({fieldsets: ['FULL]}));
});
```
ergibt folgende Ausgabe:
```js
{
  _id: "...",
  _createdDate: "...",
  _updatedDate: "...",
  activityStatus: "...",
  contactDetails: {
    ...
  }
}
```
Unter anderem fehlt die Addressen.

## `wix-members-backend`
Das aktuelle Mitglied wird ohne die Fieldset-Option nur teilweise zurückgegeben.
```js
import {currentMember} from 'wix-members-backend';

export async function getCurrentMember(){
  return await currentMember.getMember();
}
```
Die Funktion `getCurrentMember` gibt folgende Struktur zurück:
```js
{
  _id: "...",
  status: "UNKNOWN",
  contactId: "...",
  privacyStatus: "UNKNOWN",
  activityStatus: "UNKNOWN",
  _createdDate: "...",
  _updatedDate: "...",
  profile: {...}
}
```
Es fehlt unter anderem die E-Mail-Addresse (`loginEmail`) und die Kontaktdaten (`contactDetails`).

Bei der Registrierung muss die Profilbild-URL eine gültige Web-URL sein. Eine URL aus dem Wix-Media-Manager (`wix:image://v1/<uri>/<filename>`) kann nicht angegeben werden.

## `wix-stores`
Beim Hinzufügen von neuen Produkten müssen alle obligatorischen Textfelder angegeben werden. Sonst fügt die Methode `cart.addProducts` die Produkte nicht zum Einkaufskorb hinzu ohne dabei einen Fehler auszugeben.
```
import {cart} from 'wix-stores';

$w.onReady(function(){
  let currentProduct;
  $w('#button1').onClick(() => {
    const product = {
      productId: currentProduct['_id'],
      quantity: parseInt($w('#quantity').value),
      options: {
        choices: getChoices(),
        customTextFields: []
      }
    };
    const currentCart = await cart.addProducts([product]);
  });
}
```

## Product-Widget
Das Laden des Produkt-Widgets kann fehlgeschlagen, da der Fetch-Request `/_serverless/payment-methods-banner-settings` mit dem HTTP-Statuscode 429 (Too Many Requests) fehlschlägt.

## `wix-stores-backend`
### `createOrder`
- Das Lieferdatum muss in der Form `YYYY-MM-ddThh:mm:ss` (ISO-Datumsformat) angegeben sein. Sonst wirft die Funktion den Fehler `Failed to parse JSON or deserialize protobuf message`.
- Ohne Rechnungsanschrift wird keine Bestätigungsemail an den Kunden gesendet.

## `wix-pay-backend`
Beim Erstellen von Zahlungen können nur dreistellige Ländercodes angegeben werden. Zweistellig Ländercodes führem zum Fehler `Can't create payment. details: invalid response status
```js
export async function createOwnPayment(){
  return await wixPayBackend.createPayment({
    amount: 10,
    items: [
      {name: 'Test', price: 10}
    ],
    userInfo: {
      countryCode: 'DE'
    }
  });
}
```

## Backend-Requests
### Gateway Timeouts
Gateway Timeouts mit dem Statuscode 504 sind möglich. Im Frontend kann ein Retry implementiert werden, um Gateway-Timeouts zu umgehen.

### Ressourcenlimits
Backend-Requests haben ein Resourcenlimit. Bei Änderungen von Eingabefeldern sollten im Event-Handler möglichst keine Backend-Requests erstellt werden, da sonst das Resourcenlimit überschritten wird und die Backend-Requests mit dem HTTP-Statuscode `429` fehlschlagen.

### ArrayBuffer
ArrayBuffer werden als leeres Objekt `{}` übertragen.

## Formulare
Wenn mit der Tab-Taste nicht in der richtigen Reihenfolge durch Formularfelder navigiert werden kann, müssen die Felder erneut in den Vordergrund gebracht werden.
![Bring to front](bring-to-front.png)

Dann ist die Tabreihenfolge wieder korrekt.

## SSO (OAuth2) mit Google-Account (eigene Implementierung)
Wird beim Single-Sign-On mit Google-Accounts (OAuth2-Protokoll) die Homepage (Wix Editor) als Redirect-URI angegeben und der Code im Query-String im Frontendcode (folgendes Listing) geprüft, erhält man von der Google-API die Fehlermeldung `Invalid Grant`:
```js
//Frontend-Code Homepage
import wixLocation from 'wix-location';
import {validateCode} from 'backend/google-sso-utils.jsw';

async function signInWithSSO(code){
  const uerprofile = await validateCode(code, wixLocation.baseUrl);
  // todo further handle sign up workflow
}

$w.onReady(async function () {
  if(wixLocation.query?.code){
    await signInWithSSO(wixLocation.query.code);
  }
});
```
```js
// Backend-Code google-sso-utils.jsw
import wixData from 'wix-data';
import {createOAuthUrl, getTokens, getUserprofile} from 'backend/google-sso-utils.js';
import {logError} from 'public/utils.js';

export async function getAuthorizationUrl(redirectUrl){
    try{
      return await createOAuthUrl(redirectUrl);
    }catch(error){
       await logError(error);
       throw error;
    }
}

export async function validateCode(code, redirectUrl){
  try{
    const tokens = await getTokens(code, redirectUrl);
    return await getUserprofile(tokens, redirectUrl);
  }catch(error){
    await logError(error);
    throw error;
  }
}

export async function getGoogleUserprofile(tokens, redirectUrl){
  try{
    return await getUserprofile(tokens, redirectUrl);
  }catch(error){
    await logError(error);
    throw error;
  }
}
```

```js
// Backend-Code google-sso-utils.js
import wixData from 'wix-data';
import {google} from 'googleapis';

export async function readSettings(){
  const {items: settings} = await retryPromise(wixData.query('Settings').find());
  if(settings.length == 0)
    throw new Error('Settings not maintained');

  return settings[0];
}

async function createOAuthClient(redirectUrl){
    const {googleSsoClientId, googleSsoClientSecret} = await readSettings();
    return new google.auth.OAuth2(
      googleSsoClientId,
      googleSsoClientSecret,
      redirectUrl
    );
}

export async function createOAuthUrl(redirectUrl) {
  const GOOGLE_SCOPES = [
    'https://www.googleapis.com/auth/userinfo.profile',
    'https://www.googleapis.com/auth/userinfo.email'
  ];
  const oauth2Client = await createOAuthClient(redirectUrl);
  return oauth2Client.generateAuthUrl({
    access_type: 'offline',
    prompt: 'consent',
    scope: GOOGLE_SCOPES,
  });
}

export async function getTokens(code, redirectUrl){
  const oauth2Client = await createOAuthClient(redirectUrl);
  const {tokens} = await oauth2Client.getToken(code);
  return tokens;
}
```

Durch eine Redirect-URI, die auf eine HTTP-Funktion verweist, lässt sich dieser Fehler vermeiden. Die HTTP-Funkton `glssocallback` validiert den Code im Query-String der Redirect-URI und leitet den Nutzer an die Homepage weiter. Die Homepage enthält Access- und Refresh-Token im Query-String und verwendet diese weiter im Signup-Workflow.
```js
// Backend-Code http-functions.js

import { response, badRequest } from 'wix-http-functions';
import {logError} from 'public/utils.js';
import {getTokens} from 'backend/google-sso-utils.js';

export async function get_glssocallback(request){
  try{
      const tokens = await getTokens(request.query.code, 'https://www.domain.de/_functions/glssocallback');
      return response({
        status: 302,
        headers: {
          'Location': `https://www.domains.de?origin=google&action=signup&access_token=${encodeURIComponent(tokens.access_token)}&refresh_token=${encodeURIComponent(tokens.refresh_token)}`
        }
      });
   }catch(error){
     await logError(error);
     return badRequest({headers: {'Content-Type': 'text/plain'}, body: 'Exception occured'});
   }
}
```
Der Homepage-Frontendcode sieht wie folgt aus:
```js
import wixLocation from 'wix-location';

async function signInWithSSO(tokens){
  // todo further handle sign up workflow
}

$w.onReady(async function () {
  if(wixLocation.query?.action === 'signup'){
    await signInWithSSO({access_token: wixLocation.query.access_token, refresh_token: wixLocation.query.refresh_token});
  }
});
```

## Verkettung von Lightboxen
`Lightbox 1` öffnet `Lightbox 2`. Das Promise wird aufgelöst, sobald Lightbox 2 geöffnet wurde. Es wird nicht gewartet bis Lightbox 2 geschlossen ist. Die Antwort, die `Lightbox 2` zurückgibt, geht verloren und kann nicht in der Seite empfangen werden.
```js
// Page
$w.onReady(function(){
  $w('#button1').onClick(() => {
    wixWindow.openLightbox('Lightbox 1').then(() => console.log('Lightbox 1 closed, Lightbox 2 open'));
  });
});
```

```js
// Lightbox 1
$w.onReady(function(){
  $w('#button1').onClick(() => {
    wixWindow.openLightbox('Lightbox 2').then(response => wixWindow.lightbox.close(response));
  });
});

```js
// Lightbox 2
$w.onReady(function(){
  $w('#button1').onClick(() => {
    wixWindow.lightbox.close({name: $w('#input1).value});
  });
});
```
