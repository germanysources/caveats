# iOS Safari
Eine Fusszeile mit
```css
.footer{
  top: calc(100vh - 2px);
  transform: translateY(-100%);
  position: fixed
}
```

verschwindet, wenn das Navigationsmenü geöffnet wird.

## Fusszeile
Abstand einplanen mit margin-bottom bei der Hauptsektion, wenn die Fusszeile am Seitenende fixiert wird:
```html
<body>
  <section class="main-section">...</section>
  <footer class="footer">...</footer>
</body>
```
```css
.main-section{
  margin-bottom: 70px;
}
.footer{
  top: calc(100vh - 2px);
  transform: translateY(-100%);
  position: fixed
}
```
