# Umgebung
- Vue 2.6.14

# Contenteditable
`v-model` kann nicht an contenteditable-Felder gebunden werden.
Dazu ist die Arbeit mit der DOM-API notwendig.
```vue
<template>
  <div>
    <div id="changeable" contenteditable>
    </div>
    <a v-on:click="showText()">Show text</a>
  </div>
</template>

<script>
export default {
  beforeMount: function () {
    this.$el.querySelector('#changeable').innerHTML = 'Change that text'
  },
  methods: {
    showText: function () {
      console.log(this.$el.querySelector('#changeable').innerHTML);
    },
  },
};
</script>
```

# Rerendering
Elemente, die mit `v-for` in einer Schleife wiederholt werden, benötigen einen eindeutigen Schlüssel im `:key`-Attribut. Ohne Schlüssel kommt es beim Rerendering zum TypeError in der Funktion `sameVNode`.
So ist es richtig:
```vue
<template>
  <div v-for="(text, index) in texts" :key="index">{{ text }}</div>
</template>

<script>
export default {
  data: function () {
    return {
      texts: [
        'Hello World',
        'Hallo Welt'
      ]
    };
  }
};
</script>
```

# `data`-Methode
Die `data`-Methode darf wie im folgenden Listing nicht `null` als Resultat übergeben. Sonst kommt es beim Entfernen der Komponente zur Fehlermeldung `_data is null`.
Negativbeispiel:
```vue
<template>
  <div>Hallo Welt</div>
</template>

<script>
export default {
  data: function () {
    return null;
  }
}
</script>
```
