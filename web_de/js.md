# Stolperfallen

## Objekte
Eigenschaften lassen sich überschrieben ohne dass dies zu einem Fehler führt:
```js
var o = {a: 'a', b: 'b', a: 1};
console.log(o);//{a: 1, b: 'b'}
```

Eine Kopie kopiert nur die Werte von elementaren Eigenschaften (Number, String) der obersten Ebene. Bei Eigenschaften, die Objekte sind, wird nur die Referenz kopiert..
```js
var o1 = {a: 'a', b: 'b', c: 'c'};
var o2 = {alphabet: o1, encoding: 'utf-8'};
console.log({...o2}.alphabet == o1);
// returns true
```

## Arrays

### Sortierung
Number-Arrays werden bei der Sortierung wie Zeichenketten behandelt, wenn die `sort`-Methode ohne Vergleichs-Funktion aufgerufen wird.
```js
var a = [1, 10, 3, 2];
a.sort();
console.log(a); // -> ergibt [1, 10, 2, 3]
```

Mit Vergleichs-Funktion ergibt sich die richtige Sortierreihenfolge:
```js
var a = [1, 10, 3, 2];
a.sort((n1, n2) => n1 - n2);
console.log(a); // -> ergibt [1, 2, 3, 10]
```

## Berechnungen und Vergleichsoperatoren
```
var a = 'a';
console.log(1 + a == 'b' ? 0 : 5);// -> ergibt 5
```
1 + a bindet stärker. D. h. `'1a'` wird mit `'b'` verglichen.

## Async
Innerhalb forEach hat das `await`-Statement keine Wirkung. Der Schleifendurchlauf wird nicht pausiert, bis das Promise erfüllt ist:
```js
var arr = [];//get somehow
arr.forEach(async (e) => await callAsync(e));
```
Entweder in einer for-Schleife packen:
```js
var arr = [];//get somehow
for(let e of arr){
	await callAsync(e);
}
```
oder Promise.all zurückgeben:
```js
var arr = [];//get somehow
Promise.all(arr.map(e => callAsync(e)));
```

## Reduce
Summen lassen sich mit `reduce` nur für numerische Arrays bilden. Eine Summe über Objekteigenschaften lässt sich nicht direkt bilden.
Negativbeispiel:
```js
var o = [{a: 1, a: 2}];
console.log(o.reduce((sum, value) => sum + value.a));// -> ergibt [object Object]2
```
Positivbeispiel:
```js
var o =[{a: 1, a: 2}];
console.log(o.map(e => e.a).reduce((sum, value) => sum + value));// -> ergibt 3
```

## Parsen mit `parseInt` und `parseFloat`
Buchstaben vor der Ziffern führen zum `NaN-Ergebnis, während sie nach den Ziffern ignoriert werden
```js
console.log(parseFloat('N40.5')); // -> NaN
console.log(parseFloat('40.5N')); // -> 40.5
```

## Typescript
### CommonJS-module
Dies muss explizit in `tsconfig.json` erwähnt werden:
```json
{
	"files": [],
	"compilerOptions": {
                "outDir": "built",
                "module": "CommonJS",
                "target": "ES2020"
        }
}
```
### ESM-Module in node.js
Bei import-Statements wird die Endung `*.js` bei lokalen Modulen nicht automatisch ergänzt!
Folgendes Script lässt sich nicht mit node.js ausfuehren:
```ts
//date.ts
export function printDate(){
	console.log(new Date().toString());
}
```
```ts
//index.ts
import {printDate} from './date';

printDate();
```
da es wie folgt übersetzt wird:
```js
//date.js
export function printDate(){
	console.log(new Date().toString());
}
```
```js
//index.js
import {printDate} from './date';

printDate();
```

## http-Module
### Servermodus
Die Antwort muss explizit mit `response.end()` geschlossen werden:
```
const {createServer} = require('http');
createServer((request, response) => {
	response.end('Hello World');
}).listen(8080);
```
Sonst wartet der Client unendlich lange auf die Antwort.
