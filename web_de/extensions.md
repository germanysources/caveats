# Chrome extensions
## Umgebung
- Manifest Version v2 und v3
- Google Chrome Version 97

## Meldung an tab senden
Nur mit `chrome.tabs.sendMessage` möglich, nicht mit `chrome.runtime.sendMessage`

## Limitierung
`chrome.runtime.sendMessage` kann nur 128MB übertragen. Größere Nachrichten führen zum Fehler `Maximum message size exceeded`

## Storage
```js
var now = {d: new Date()};
chrome.storage.local.set(now);
```
Das Datum wird als `{}` synchronisiert und abgelegt im Speicher.

## Cookies
Nur in Service-Workern/Background-Scripts abrufbar mit dem Objekt `chrome.cookies`, nicht in Content-Scripts.

## Content-Scripts
Nur
- `chrome.runtime`
- `chrome.storage`
- `chrome.csi`
- `chrome.dom`
- `chrome.extension`
- `chrome.i18n`
- `chrome.loadTimes`
ist verfügbar.

Die JavaScript-Objekte auf der Webseite und die JavaScript-Objekte im Content-Script sind getrennt. Nur das DOM wird geteilt.

Webcomponents lassen sich nicht nativ wie in einer Webseite nutzen. Ein Polyfill muss verwendet werden.

## Ports bei Manifest v3
Ports beim Google Chrome Browser werden nach [5 Minuten inaktivität](https://stackoverflow.com/questions/66618136/persistent-service-worker-in-chrome-extension) automatisch geschlossen. Ein Heart-Beat kann dies verhindern:
```js
chrome.runtime.onConnect.addListener(function (port) {
        if (port.name !== 'heartbeatport')
                return;

        const intervalId = setInterval(() => {
                port.postMessage({heartbeat: true});
        }, 5 * 60 * 1000 - 5 * 1000);

        port.onMessage.addListener(console.log);
});
```

## Kommunikation zwischen Service-Workern (Background-Scripts) und Tabs
Das erste Content-Script, das mit `sendResponse` antwortet, bestimmt die Antwort. Antworten, die später gesendet werden, ignoriert der Service-Worker bzw. das Background-Script. Folgendes Content-Script sendet immer eine leere Antwort.
```js
chrome.runtime.onMessage((request, sender, sendResponse) => {
  sendResponse();
});
```

Im Background-Script kommt immer `null` als Antwort an, egal was die anderen Content-Scripte später senden.
```js
chrome.tabs.sendMessage(tabId, {action: 'query'}, response => console.assert(response == null));
```

## Asynchrone Funktionen bei der Kommunikation zwischen Service-Workern (Background-Scripts) und Tabs
Die Callback-Funktion, die auf eine Nachricht reagiert, darf keine asynchrone Funktion sein. Sonst kommt es zur Ausnahme `Port was closed before a response was received`.
Negativbeispiel:
```js
chrome.runtime.onMessage.addListener(async (request, sender, sendResponse) => {
  doSomeAsyncStaff().then(sendResponse).catch(console.error);
  return true;
});
```
Positivbeispiel:
```js
chrome.runtime.onMessage((request, sender, sendResponse) => {
  doSomeAsyncStaff().then(sendResponse).catch(console.error);
  return true;
});
```

Wenn die Antwort asynchron erfolgt, muss die Callback-Funktion `true` zurückgeben.

## Storage
`localStorage` und `sessionStorage` wird zwischen Content-Scripten und den Page-Scripten (Page-Scripte sind Scripte, die im DOM eingebunden sind) geteilt. 
```html
<!-- Page Script -->
<script src="path/index.js"></script>
```

## Fehler
`window.onerror` wird nicht bei Fehlern in Content-Scripten getriggert.
