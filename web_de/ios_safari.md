## Hover
iOS Safari reagiert nicht auf die CSS-Pseudoklasse `:hover`.

## HTML-Anchor-Elements
Das `href`-Attribut liefert nur den String, der im HTML-Text angegeben wurde und nicht wie Firefox oder Chromium-Browser die vollständige URL.
```html
<a href="../index.html"></a>
<script>
  console.log(document.querySelector('a').href);
  // Safari: ../index.html
  // Firefox, Chromium: http://host/index.html
</script>
```
