# HTMLElement
## HTMLElement.offsetHeight
Ist 0, wenn Element ausgeblendet (HTMLElement.style.display === 'none').

## HTMLElement.innerHTML
Eine Zuweisung an `HTMLElement.innerHTML` erzeugt immer neue DOM-Nodes.
Bei einer Zuweisung `HTMLElement.innerHTML += '<div>a</div>'` gehen alle Events und Funktionen verloren, die an die bisherigen DOM-Nodes gebunden wurden.

## HTMLElement.classList
Eine leerer String hat die Ausnahme ` DOMTokenList.add: The empty string is not a valid token.` zur Folge.
```js
element.classList.add(''); //-> Error
```

# WebComponents

## ShadowDom
ShadowDom und das Wurzeldokument (`document`) bilden jeweils einen eigenen Kontext, bei dem die folgenden Funktionen nur für die DOM-Knoten des jeweiligen Kontextes verfügbar sind:
- `querySelectorAll` sucht nicht in eingebetteten ShadowDoms, sondern nur im aktuellen Kontext. `document.querySelectorAll('a')` wird keine Links in ShadowDoms finden.
- `MutationObserver` kann nur innerhalb des eigenen Kontextes Änderungen überwachen. Eine Überwachung von eingebetteten ShadowDoms ist nicht möglich. Im folgenden Listing wird der `MutationObserver` nicht anschlagen, wenn im ShadowDom Änderungen passieren.
```js
new MutationObserver(console.log).observe(document.body, {childList: true, subtree: true})
```
- `ShadowRoot` hat kein style-Attribut wie andere HTML-Elemente.

## Cache
Die Children einer Web-Komponente können noch im Cache vorhanden sein, wenn connectedCallback mehrmals gerufen wird.
```
class HelloWorld extends HTMLElement{
	connectedCallback(){
		const span = document.createElement('span');
		span.innerHTML = 'Hello World';
		this.appendChild(span);
	}
}
customElements.define('x-hw', HelloWorld);
document.body.appendChild(document.createElement('x-hw'));
document.body.innerHTML = '<x-hw></x-hw>'
```
Dies gibt `Hello World` doppelt aus.

## Klassenkonstruktor
Web-Components müssen als Klasse erstellt werden. Eine Erstellung mittels Prototypen ist nicht möglich:
```
var wc = function(){ this.#main = document.createElement('div') }
wc.prototype.connectedCallback = function(){ this.appendChild(this.#main) }
```

# Fetch_API
Status-Code ist `0` wenn einem Redirect nicht gefolgt wird und der HTTP-Status-Code 302 ist:
```
fetch(`resource`, {redirect: 'manual'}).then(r => console.log(r.status))
// gives 0
```

# FormData
Der Zeichensatz UTF-8 wird zur Übertragung verwendet. Der Header `Content-Type` erhält keinen Zusatz mit dem Zeichensatz:
```
const form = new FormData();
form.set('foo', 'bar');
await fetch('', {
	method: 'POST',
	body: form
})
// HTTP-Header bei Multipart-Form
// Content-Type: multipart/form-data; boundary=---------------------------320533149336498637461834369565
```

# Selection-API
## Google Chrome
`document.getSelection()` gibt `body` als anchorNode und focusNode zurück, wenn die Selektion innerhalb des ShadowDoms liegt (https://bugs.chromium.org/p/chromium/issues/detail?id=380690).

# Templates
Nach Klonen und Einfügen eines Templates ist das Dokumentfragment leer.
Template:
```html
<template>
  <div id="title">Titel</div>
</template>
```
```js
const clone = document.querySelector('template').content.cloneNode();
document.body.appendChild(clone);
console.log(clone.querySelector('#title'));// -> gibt null aus
```

#  `document.open`
`document.open` sorgt dafür, dass alle DOM-Knoten, die dynamisch nach dem Laden der Seite mit JavaScript eingefügt wurden, wieder entfernt werden. Dies betrifft auch dynamisch eingefügte Skripte. In den Developer-Tools (Source-Folder) erscheinen die Skripte unter Umständen nach der Entfernung noch. In Service-Workern von Browsererweiterung macht sich der Aufruf der Methode `document.open` wie folgt bemerkbar:
```js
chrome.tabs.onUpdated.addListener((tabId, changeInfo, tab) => {
  console.log(changeInfo); // changeInfo enthaelt nur das title-Attribut
});
```

# Entfernen aller Kindknoten
Die folgende For-Schleife entfernt die Kindknoten aus dem DOM nur unvollständig:
```js
for(const node of parent.childNodes){
  node.remove();
}
```
Um alle Knoten zu entfernen, sollte die For-Schleife wie folgt modifiziert werden:
```js
for(const node of [...parent.childNodes]){
  node.remove();
}
```

## Kindknoten
Das children-Attribut enthält keine Textknoten. Diese sind nur im childNodes-Attribut enthalten.

# Structured Clone Algorithmus
Nicht alle JavaScript-Objekte lassen sich mit dem [Structured Clone Algorithmus](https://developer.mozilla.org/en-US/docs/Web/API/Web_Workers_API/Structured_clone_algorithm) übertragen:
- DOM-Knoten, Funktionen und Error-Objekte führen zur DATA_CLONE_ERR-Exception

Einige Parameter gehen verloren:
- `lastIndex`-Feld von `RegExp`-Objekten
- Property descriptors, setters and getters werden nicht serialisiert. Wenn ein Objekt als Read-Only markiert ist, ist es nach der Serialisierung und Deserialisierung nicht mehr Read-Only.
- Die Prototyphierarche wird nicht dupliziert

Certain parameters of objects are not preserved:
- The lastIndex field of RegExp objects is not preserved.
- Property descriptors, setters, and getters (as well as similar metadata-like features) are not duplicated. For example, if an object is marked read-only using a property descriptor, it will be read-write in the duplicate, since that's the default condition.
- The prototype chain does not get walked and duplicated.

Der Algorithmus kommt unter anderem bei `window.postMessage` zum Einsatz, um Meldungen zu übertragen.

# Funktionsnamen
## `open`
Nicht die eigene `open`-Funktion, die im folgenden Listing deklariert ist, kommt zur Anwendung, sondern `document.open`. Besser ist es einen anderen Funktionsnamen zu wählen.
```js
<html>
  <body>
    <script>
      function open(){
        document.querySelector('input').showPicker();
      }
    </script>
    <input type="file" style="display:none">
    <button onclick="open()">Open</button>
  </body>
</html>
```

# Eingabeelemente
Deaktivierte Eingabeelemente reagieren nicht auf Klick-Ereignisse. In folgenden Listing wird der Ereignisbehandler nach einem Mausklick nicht gerufen.
```html
<input type="text" disabled onclick="() => console.log('disabled element clicked')">
```

# Crypto API
## Import Key
- Beim Import eines öffentlichen RSA-Schlüssels im `SubjectPublicKeyInfo`-Format muss der Hash-Algorithmus mit angegeben werden, sonst kommt es zur Ausnahme `DOMException: Data provided to an operation does not meet requirements`.
Negativbeispiel:
```js
crypto.subtle.importKey('spki', new Uint8Array(key), {name: 'RSA-OAEP'}, false, ['encrypt']);
```

## Verschlüsseln
Bei der Verschlüsselung mit dem RSA-OAEP-Algorithmus ist die Nachrichtenlänge begrenzt auf `kLen - 2 * (hLen + 1)` (`kLen` ist die Schlüssellaenge in bytes, `hLen` ist die Länge des Digits-Algorithmuses (z.B. SHA-256 -> `hLen = 32`, maximale Nachrichtenlänge `256 - 2 * (32 + 1) = 190`bytes.
Bei Überschreitung kommt es zur Fehlermeldung `DOMException: The operation failed for an operation-specific reason`.

