# span
Ausrichtung immer am unteren Ende des Elternelements.

# table
Höhenbegrenzung und Breitenbegrenzung mit Scrollleiste beachtet die Tabelle nicht:
```html
<table style="height: 50px;overflow-y: scroll">
<tr><td>1</td></tr>
</table>
```
Stattdessen muss die Tabelle mit einem div-Elemente eingewickelt werden:
```html
<div style="height: 50px;overflow-y: scroll">
<table>
<tr><td>1</td></tr>
</table>
</div>
```

# div
Füllt standardmässig die gesamte Breite inklusive der vertikalen Scrollleiste.

# Pseudo-Elemente
## svg, img, hr
Die Pseudo-Elemente `::before` und `::after` werden nicht vor bzw. nach svg-, img- und hr-Knoten dargestellt. Sie werden nur vor bzw. nach Textelementen dargestellt.

## Content
Die Pseudo-Elemente `::before` und `::after` müssen ein content-Attribut besitzen, sonst werden sie nicht dargestellt.

## Opacity
Die CSS-Eigenschaft `opacity` wird ignoriert in Pseudo-Elementen:
```css
.class::before{
  opacity: 0.5;
}
```

# Herstellerspezifische Erweiterungen (-moz, -webkit)
Erweiterungen mehrerer Hersteller nicht mit ODER verknuepfen:
```css
-webkit-scrollbar, -moz-scrollbar{
  color: blue;
}
```
Stattdessen separate Regeln definieren:
```css
-webkit-scrollbar{
  color: blue;
}
-moz-scrollbar{
  color: blue;
}
```

# Selektoren
`+` bedeutet zwei aufeinanderfolgende Elemente.

```css
.class1 + .class2{
	background: green;
}
```
```html
<div class="class1">
<div class="class2">
```
`<div class="class2">` erhält grünen Hintergrund.

# svg
Y-Koordinate läuft von oben nach unten.

# Höhenbegrenzung absolut positioniertes Element
Muss die Höhe innerhalb eines absoluten positionierten Elementes begrenzt werden, ist eine Flexbox notwendig.
```html
<html>
  <head>
    <style>
      .abs-element{
        position: absolute;
        height: 80vh;
        /* Flexbox notwendig*/
        display: flex;
        flex-direction: column;
      }
      .intermediate-container{
        /* Flexbox notwendig*/
        display: flex;
        flex-direction: column;
        overflow-y: hidden;
      }
      .long-list{
         overflow-y: scroll;
      }
    </style>
  </head>
  <body>
    <div class="abs-element">
      <div>Titel</div>
      <div class="intermediate-container">
        <div>A long list</div>
        <ul class="long-list">
          <li>...</li>
          <!-- ... -->
        </ul>
      </div>
    </div>
  </body>
</html>
```
Ohne Flexbox wächst die Liste `<ul class="long-list">` über die Grenzen des Fensters hinaus.

## Events
### `event.preventDefault()`
Wird im `mousedown`-Event `event.preventDefault()` gerufen, sind Eingabeelemente unterhalb des DOM-Kontens nicht mehr fokusierbar.
```html
<html>
  <body>
    <div id="a"><input></div>
    <script>
      document.querySelector('#a').addEventListener('mousedown', event => event.preventDefault());
    </script>
  </body>
</html>
```
Das Textbox im Knoten `<div id="a">` ist nicht mehr fokusierbar. Abhilfe schafft folgender Event-Handler:
```html
<html>
  <body>
    <div id="a"><input></div>
    <script>
      document.querySelector('input').addEventListener('mousedown', event => event.target.focus());
      document.querySelector('#a').addEventListener('mousedown', event => event.preventDefault());
    </script>
  </body>
</html>
```

## Pseudo-Klasse `:valid`
Style:
```css
input:valid{
  border: 3px solid green;
}
```
Bei deaktivierten (disabled) Eingabeelementen wird mit Setzen eines Wertes die Pseudo-Klasse `:valid` nicht aktiviert. Das Eingabe-Element im folgenden Beispiel erhält keinen grünen Rahmen:
```js
var input = document.createElement('input');
input.required = true;
input.disabled = true;
input.value = 'Hello World';
```

## Grid
### Grid Template Areas
Grid Template Areas müssen in jeder Zeile dieselbe Spaltenanzahl besitzen. Im folgenden Beispiel ist eine ungültige Angabe (in der ersten Zeile 2 Spalten, in der zweiten Zeile 3 Spalten) enthalten. Mit dieser Vorgabe erstellen die Browser kein Gitter.
```js
var grid = document.createElement('div');
grid.style.display = 'grid';
grid.style['grid-template-areas'] = "'A1 B1' 'A2 B2 C2'";
```
