# Umgebung
- Java 11

# Berechnungen:
## Long und Int
```
int b = 5;
long a = 1000000 * b;
```
Das Ergebnis hat die Größe eines ints und wenn die Multiplikation die int-Grenzen uebersteigt, wird es negativ.
## Durch 0 teilen
Eine Teilung durch 0 wirft keine Ausnahme. Das Ergebnis ist unendlich.

# Strings: niemals direkt vergleichen mit dem ==-Operator
```java
String a = "a"
System.out.println(a == "a") //-> gibt false aus
```
immer mit der Equals-Method vergleichen:
```java
System.out.println(a.equals("a")); // -> gibt true aus
```

# Beans:
```java
class MyBean{
   private Strign name;
   public String getName(){
     return name;
   }
   public void setName(String name){
     this.name = name;
   }
}
MyBean mb = new MyBean();
Expression e = new Expression(mb, "getName", new Object[0]);
e.execute();
```
`e.execute()` wirft `java.lang.NoSuchMethodException`, daher die Methode dynamisch aufrufen:
```java
String name = (String)MyBean.class.getMethod("getName").invoke();
```

# Listen:
```java
List<String> l = new ArrayList<String>();
//fill list
```
`l.toArray(new String[0])` wählen anstatt `l.toArray()`

# Arrays instanziieren:
```java
class MyBean{
   private Strign name;
   public String getName(){
     return name;
   }
   public void setName(String name){
     this.name = name;
   }
}
MyBean beans[] = new MyBean[2];
```
instanziiert nicht die Klassen, sondern reserviert nur Platz fuer zwei Referenzen.
Jede Position im Array muss noch instanziiert werden:
```java
beans[0] = new MyBean();
beans[1] = new MyBean();
```

# Reguläre Ausdrücke
## Muster
Die Regulären Ausdrücke `a.+b` und `a[.\\v\\h]+b` erkennt den String `af\nb` wegen des Zeilenumbruchs nicht als Match.
Der regulären Ausdruck `a[\\w\\v\\h]+b` dagegen schon.

## `java.util.regex`-Paket
Ohne Aufruf der Methode `find()` wirft die Methode `group()` die Ausnahme `IllegalStateException`.
Negativbeispiel:
```java
Matcher matcher = Pattern.compile("(\\d+)").matcher("abc-45600");
System.out.println(matcher.group());
```
Positivbeispiel:
```java
Matcher matcher = Pattern.compile("(\\d+)").matcher("abc-45600");
matcher.find();
System.out.println(matcher.group());
```

# `ClassLoader`
Ab Java 1.9 funktioniert der Cast von `ClassLoader.getSystemClassLoader()` zu `URLClassLoader` nicht mehr:
```java
URLClassLoader classLoader = (URLClassLoader)ClassLoader.getSystemClassLoader();
```
