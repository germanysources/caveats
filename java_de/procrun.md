## Hintergrunddienste unter Windows mit Procrun
Wird als Dienstbenutzer der lokale Dienstbenutzer gewählt kommt es beim Starten des Dienstes zu folgender Fehlermeldung:
```
[2024-02-19 15:49:54] [error] [23532] apxServiceControl(): dwState(4 = SERVICE_RUNNING) != dwCurrentStat
e(1 = SERVICE_STOPPED); dwWin32ExitCode = 0, dwWaitHint = 2000 millseconds, dwServiceSpecificExitCode =
0
[2024-02-19 15:49:54] [error] [23532] Zugriff verweigert
[2024-02-19 15:49:54] [error] [23532] apxServiceControl(): returning FALSE
[2024-02-19 15:49:54] [error] [23532] Zugriff verweigert
[2024-02-19 15:49:54] [error] [23532] Failed to start service 'Waageserver10'.
[2024-02-19 15:49:54] [error] [23532] Zugriff verweigert
[2024-02-19 15:49:54] [error] [23532] Apache Commons Daemon procrun failed with exit value: 5 (failed to
 start service).
[2024-02-19 15:49:54] [error] [23532] Zugriff verweigert
Zugriff verweigert
Failed to start service.
```

Mit dem lokalen Systemkonto lässt sich dieser Fehler umgehen.

## Dienst starten
Wird beim Start im Java-Programm eine Exception geworfen, kommt es beim Start zur Fehlermeldung `apxServiceControl(): dwState(4 = SERVICE_RUNNING) != dwCurrentState(1 = SERVICE_STOPPED); dwWin32ExitCode = 1066, dwWaitHint = 2000 millseconds, dwServiceSpecificExitCode = 4`. Die Fehlermeldung ist in den Logs protokolliert. Das Log-Verzeichnis kann im Parameter `--LogPath` angegeben werden.
