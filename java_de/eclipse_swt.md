# Eclipse SWT
## Rerendering Composites (`org.eclipse.swt.widgets.Composite`)
Ein nachträgliches Hinzufügen/Entfernen von Komponenten zum Composite bewirkt kein automatisches Rerendering. Um das Rerendering anzustossen, muss die Methode `composite.layout()` gerufen werden. Die Methoden `composite.redraw` und `composite.update` bewirken kein Rerendering.
### Dialoge
Beim Rerendering bleibt die Größe des Dialogs unverändert, wenn der Dialog nicht in der Größe änderbar ist. Die Komponenten ändern ihre Größe in diesem Fall.
