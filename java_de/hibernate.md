# Hibernate Stolperfallen

In den folgenden Beispielen wurde die `javax.persistence`-Bibliothek verwendet.

## `persistence.xml`
```xml
<persistence version="2.1"
             xmlns="http://xmlns.jcp.org/xml/ns/persistence"
             xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
             xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/persistence
             http://xmlns.jcp.org/xml/ns/persistence/persistence_2_1.xsd">

    <persistence-unit name="MAIN" transaction-type="RESOURCE_LOCAL">
        <properties>
	    <property name="hibernate.connection.datasource"
		      value="java:/comp/env/jdbc/TESTDB"/>
            <property name="hibernate.dialect"
                      value="org.hibernate.dialect.MariaDBDialect"/>

            <property name="hibernate.hbm2ddl.auto" value="update"/>
            <property name="hibernate.show_sql" value="true" />
            <property name="debugUnreturnedConnectionStackTraces"
                      value="true" />
        </properties>
    </persistence-unit>
</persistence>
```
`<property name="hibernate.hbm2ddl.auto" value="update"/>` legt Tabellen an, die noch nicht existieren. Die Spaltendefinition von bereits existieren Tabellen wird nicht in allen Fällen automatisch mit geändert, wenn sich die Entitätsklasse ändert.

## Fehlermeldung `No Persistence provider for EntityManager named ...`
Neben Schreibfehlern des Persistenz-Unitnamens, des Dateinamens `persistence.xml` oder der falschen Lokation kann auch korrupter Bytecode zu diesem Fehler führen.
Ob Bytecode korrupt ist, findet man bei der direkten Instanziierung des Persistenzprovider wie im folgenden Listing heraus.
```java
Map<String, String> properties = new HashMap<String, String>();
properties.put("hibernate.connection.datasource", "java:/comp/env/jdbc/TESTDB");
properties.put("hibernate.dialect", "org.hibernate.dialect.SQLServerDialect");
properties.put("hibernate.hbm2ddl.auto", "validate");
properties.put("hibernate.show_sql", "true");
new HibernatePersistenceProvider().createEntityManagerFactory("MAIN", properties); 
```

Korrupter Bytecode wirft ein `java.lang.VerifyError: StackMapTable error: bad offset`

## Ungeschlossene Verbindungen
Wird eine Transaktion nicht abgeschlossen, bleibt die Verbindung weiterhin geöffnet:
```java
EntityManager em = ...//get somehow
em.getTransaction().begin();
//kein close erfolgt
```

## Entity-Cache
Um keine Memory-Leaks im Entity-Cache zu bekommen, sollten die Entitäten die Methoden `equals` und `hashCode` überschreiben.

## Limit-Clause
The SQL-Limit-Clause wird von HSQL nicht unterstützt.

## Annotationen
Annotationen müssen entweder alle über den Feldern (Field access type) oder über den Gettern (Property access type) stehen. Ein Mix führt zum Fehler `org.hibernate.MappingException: Could not determine type for ...`.

## Primärschlüssel
Gegeben sei folgende Primärschlüsseldefinition (Automatische Primärschlüsselermittlung):
```java
@Entity
@Table(name="EMPLOYEE")
public class Employee{

  @Id
  @GeneratedValue(generator="increment")
  @GenericGenerator(name="increment", strategy = "increment")
  private Long id;
  // ...

}
```
Nur beim ersten Insert wird der Index neu ermittelt und danach zählt Hibernate diesen als Programmvariable hoch. Fügt eine andere Applikation einen Datensatz in die Tabelle `EMPLOYEE` ein, versucht Hibernate denselben Primärschlüssel erneut einzufügen.

## `SELECT`-Statements
Gegeben sei folgende Definition:
```java
@Entity
@Table(name="COMPANY")
public class Company{

  @Id
  @GeneratedValue(generator="increment")
  @GenericGenerator(name="increment", strategy = "increment")
  private Long id;
  private String name;

}
```
```java
@Entity
@Table(name="EMPLOYEE")
public class Employee{

  @Id
  @GeneratedValue(generator="increment")
  @GenericGenerator(name="increment", strategy = "increment")
  private Long id;
  private Company company;

}
```

Das folgende Statement führt zum Fehler `java.lang.IllegalStateException: org.hibernate.TransientObjectException: object references an unsaved transient instance - save the transient instance before flushing`, da der Primärschlüssel der `Company`-Instanz undefiniert ist:
```java
EntityManager em = ... // get somehow
Company company = new Company();
company.setName("My company");
Query query = em.createQuery("select e from Employee where company = ?1");
query.setParameter(company);
(List<Employee>)query.getResultList();
```
Um den Fehler zu vermeiden, muss der Primärschlüssel angegeben werden:
```java
EntityManager em = ... // get somehow
Company company = new Company();
company.setId(1);
Query query = em.createQuery("select e from Employee where company = ?1");
query.setParameter(company);
(List<Employee>)query.getResultList();
```

## Aggregatsfunktionen
Gegeben Sie folgende Entität:
```java
@Entity
@Table(name="WORKING_HOURS")
public class WorkingHours{

  @Id
  @GeneratedValue(generator="increment")
  @GenericGenerator(name="increment", strategy = "increment")
  private Long id;
  private Employee employee;
  private Date date;
  private float hours;

}
```
Das SELECT-Statement `select new com.caveats.WorkingHoursDTO(employee, sum( hours )) from WorkingHours group by employee` führt zur Ausnahme
```
Caused by: java.lang.NullPointerException
at org.hibernate.dialect.function.StandardAnsiSqlAggregationFunctions$SumFunction.determineJdbcTypeCode(StandardAnsiSqlAggregationFunctions.java:145)
at org.hibernate.dialect.function.StandardAnsiSqlAggregationFunctions$SumFunction.getReturnType(StandardAnsiSqlAggregationFunctions.java:157)
```
Die Entität muss einen Aliasnamen bekommen, um den Fehler zu verhindern:
```
select new com.caveats.WorkingHoursDTO(w.employee, sum( w.hours )) from WorkingHours w group by w.employee
```

## Fremdschlüssel
Es ist nicht möglich eine Fremdschlüsselbeziehung zu erstellen mit einer Entität, die mehr als 1 Schlüsselfeld besitzt wie in den folgenden Listings. In der Klasse `UserRole` wird versucht eine Fremdschlüsselbeziehung zur Klasse `Roles` zu erstellen. Hibernate wirft in diesem Fall den Fehler `A Foreign key refering Roles from UserRole has the wrong number of column. should be 2`.
```java
@Data
@Entity
@Table( name = "USER_ROLES" )
public class UserRole implements Serializable{

    @Id
    private String email;
    @Id
    @JoinColumn(name="rolename")
    @ManyToOne
    private Roles role;

}
```
```java
@Data
@Entity
@Table( name = "ROLES" )
public class Roles{

    @Id
    private String id;
    @Id
    private String language;
    private String label;
}
```

## JSON-Serialisierung
Die JSON-Serialisierung einer `User`-Instanz mit der Jackson-Databind-Bibliothek führt aufgrund der ManyToMany-Assoziation im Lazy-Fetch-Mode zum Fehler `com.fasterxml.jackson.databind.JsonMappingException: failed to lazily initialize a collection of role: User.roles, could not initialize proxy - no Session (through reference chain: java.util.ArrayList[0]->User["roles"]`. Intern kommt es zu einer Endloss-Rekursion. Die `UserRole`-Instanz verweist auf die `User`-Instanz, die `User`-Instanz wieder auf die `UserRole`-Instanz.
Negativbeispiel:
```java
@Data
@Entity
@Table( name = "USER_ROLES" )
public class UserRole implements Serializable{

    @Id
    private String email;
    @Id
    @JoinColumn(name="rolename")
    @ManyToOne
    private Roles role;

}
```
```java
@Data
@Entity
@Table( name = "USER" )
public class User{

  @Id
  private String username;
  @ManyToMany
  private List<UserRole> roles;

}
```

Im Eager-Fetch-Mode kann dieser Fehler verhindert werden.
Positivbeispiel:
```java
@Data
@Entity
@Table( name = "USER" )
public class User{

  @Id
  private String username;
  @ManyToMany(fetch=FetchType.EAGER)
  private List<UserRole> roles;

}
```

Es ist aber nicht möglich, dass eine Entität mehrere ManyToMany-Assoziationen im Eager-Fetch-Mode enthält. Bei mehreren ManyToMany-Assoziationen mit Eager-Fetch-Mode kommt es zum Fehler `org.hibernate.loader.MultipleBagFetchException: cannot simultaneously fetch multiple bags`.

## MariaDB
Wird wie im folgenden Listing eine Transaktion nicht abgeschlossen, ist bei Löschoperationen die gesamte Tabelle gesperrt. Es kommt beim Versuch eine neue Zeile einzufügen zur Fehlermeldung `Lock wait timeout exceeded; try restarting transaction`.
```java
EntityManager em = createEntityManagerSomeHow();
em.getTransaction().begin();
Query deleteStatement = em.createQuery("delete from User where username = ?");
deleteStatement.setParameter(1, username);
deleteStatement.executeUpdate();
// no commit or rollback
```
Um die Sperre aufzuheben, sollte die Datenbank neu gestartet werden.
