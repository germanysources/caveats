# Tabellen

## Farben
Bei Selektion einer Tabellenzeile bleibt die Hintergrundfarbe bei folgendem TableCellRenderer unverändert:
```java
public JTable getTable(TableModel tableModel){
  JTable table = new JTable(tableModel);
  table.setDefaultRenderer(String.class, new ColorRenderer());
}
private class ColorRenderer implements TableCellRenderer{
  public ColorRenderer(){
    super();
  }

  @Override
  public Component getTableCellRendererComponent(JTable table, Object field, boolean isSelected, boolean hasFocus, int row, int column){
    setBackground(java.awt.Color.GREEN);
    setText(field.toString());
    return this;
  }
```

Um selektierte Zellen farblich hevorzuheben, kann die Klasse `ColorRenderer` die Klasse `javax.swing.table.DefaultTableCellRenderer` beerben und bei selektierten Zellen die super-Methode aufrufen.
```java
public JTable getTable(TableModel tableModel){
  JTable table = new JTable(tableModel);
  table.setDefaultRenderer(String.class, new ColorRenderer());
}
private class ColorRenderer extends Default TableCellRenderer{
  public ColorRenderer(){
    super();
  }

  @Override
  public Component getTableCellRendererComponent(JTable table, Object field, boolean isSelected, boolean hasFocus, int row, int column){
    if(isSelected){
      return super.getTableCellRendererComponent(table, field, isSelected, hasFocus, row, column);
    }else
      setBackground(java.awt.Color.GREEN);
      setText(field.toString());
    }
    return this;
  }
```
