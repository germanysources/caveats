# Umgebung
- Java 11

# Bildverarbeitung mit dem Paket `javax.imageio`

## Schreiben von Bildern mit Klasse `ImageIO`
Als Format (Parameter `formatName`) sind nur die Namen erlaubt, die die Methode `getWriterFormatNames` zurückgibt. Z.B.:
- `jpg`
- `png`
- `jpeg`

MIME-Typen wie `image/jpeg` sind ungültig. Das Bild wird nicht geschrieben. Dabei wirft die Methode 
`write` keine `IOException`, sondern gibt `false` zurück.

```java
public static boolean write(RenderedImage im,
                            String formatName,
                            OutputStream output)
                     throws IOException
```
