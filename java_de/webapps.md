# JSP

```jsp
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"\
 prefix="c" %>
<c:forEach items=${items} var="item">
<div>${item.getProp()}
</c:forEach>
```
Es können nur get-Methoden gerufen werden, Attribute führen wie im folgenden Listing zu Laufzeitfehlern:
```
<c:forEach items=${items} var="item">
  <div>${item.attribute}</div>
</c:forEach>
```

Alle Arrays und Kollektionen, die mit einer forEach-Schleife durchlaufen werden, müssen als pageContext-Attribut gesichert werden:
```java
List<MyBean> items = ...//get list somehow
pageContext.setAttribute("items", items);
```

## Tags
### Variablen
Variablen, die in Code-Abschnitten definiert wurden, können nicht als Attribute an Subtags übergeben werden. Folgendes Listing führt zu einem Laufzeitfehler
```jsp
<%@tag import="java.util.List,java.util.ArrayList"%>
<%
  List<String> countries = new ArrayList<String>();
  countries.add("DE");
  countries.add("AT");
  countries.add("CH");
%>
<%@taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<h2>DACH Region</h2>
<tags:countries countries="${countries}"/>
```

### Attribute
Aktualparameter müssen in geschweiften Klammern angegeben werden mit führenden Dollarzeichen (`${countries}`). `<%=countries%>` wird zur Laufzeit als String übergeben.

# Connection pool (`context.xml`)
## Endloses warten
```xml
<?xml version="1.0" encoding="utf-8"?>
<Context>
  <Resource name="jdbc/TESTDB"
            auth="Container"
            type="javax.sql.DataSource"
            driverClassName="org.mariadb.jdbc.Driver"
            url="jdbc:mariadb://instance:3306/ea?useUnicode=yes&amp;characterEncoding=UTF-8"
            username="root"
            password="abc"
            factory="org.apache.tomcat.dbcp.dbcp2.BasicDataSourceFactory"/>
</Context>
```
Der Connection-Pool wartet in dieser Konfiguration bis eine Verbindung an den Pool zurückgegeben wird, wenn keine offene Verbindung mehr vorhanden ist. Wird eine Verbindung nicht richtig geschlossen, wartet er unendlich lange (Option `maxWaitMillis` verhindert das unendlich lange Warten).
```xml
<?xml version="1.0" encoding="utf-8"?>
<Context>
  <Resource name="jdbc/TESTDB"
            auth="Container"
            type="javax.sql.DataSource"
            driverClassName="org.mariadb.jdbc.Driver"
            url="jdbc:mariadb://instance:3306/ea?useUnicode=yes&amp;characterEncoding=UTF-8"
            username="root"
            password="abc"
            factory="org.apache.tomcat.dbcp.dbcp2.BasicDataSourceFactory"
            maxWaitMillis="40000"/>
</Context>
```

## Verbindung werden von der Datenbank geschlossen
Wenn die Datenbank aktive Verbindungen nach einem Timeout schliesst, bekommt das der Pool nicht mit und leiht eine geschlossene Verbindung an die Applikation aus (diese wirft dann einen Fehler `connection rest by peer`):
```xml
<Context>
  <Resource name="jdbc/TESTDB"
            auth="Container"
            type="javax.sql.DataSource"
            driverClassName="org.mariadb.jdbc.Driver"
            url="jdbc:mariadb://instance:3306/bt?useUnicode=yes&amp;characterEncoding=UTF-8"
            username="root"
            password="abc"
            factory="org.apache.tomcat.jdbc.pool.DataSourceFactory"
            maxWaitMillis="10000"
            minIdle="30"
            maxIdle="300"
            removeAbandonedOnBorrow="true"
            logAbandoned="true"/>
  </Resource>
</Context>
```
Hier sollte die Option `testOnBorrow="true"` gesetzt werden mit einer Validierungsabfrage (`validationQuery`):
```xml
<Context>
  <Resource name="jdbc/TESTDB"
            auth="Container"
            type="javax.sql.DataSource"
            driverClassName="org.mariadb.jdbc.Driver"
            url="jdbc:mariadb://instance:3306/bt?useUnicode=yes&amp;characterEncoding=UTF-8"
            username="root"
            password="abc"
            factory="org.apache.tomcat.jdbc.pool.DataSourceFactory"
            maxWaitMillis="10000"
            minIdle="30"
            maxIdle="300"
            removeAbandonedOnBorrow="true"
            logAbandoned="true"
            testOnBorrow="true"
            validationQuery="SELECT 1"/>
  </Resource>
</Context>
```
Damit werden geschlossene Verbindungen aus dem Pool entfernt.

# Request Weiterleitung
Die folgende Anweisung übergibt die Ressource `/foo.jsp`. Es erfolgt keine Weiterleitung mit dem Statuscode 302. Die Weiterleitung bleibt dem Client verborgen.
```java
@Override
protected void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException,IOException{
        ServletContext servletContext = getServletConfig().getServletContext();
        servletContext.getRequestDispatcher("/foo.jsp").forward(request, response);
    }
```
Wichtig ist, eine absolute URI anzugeben (immer mit `/` als ersten Buchstaben).

# JS-Template-Strings
Diese können nicht verwendet werden, wenn die JSTL-Core-Library verwendet wird, da `${...}` als Java-Statement interpretiert wird.
```jsp
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
  <head>
    <script>
      // nicht moeglich
      var date = `${date}T${time}`;
    </script>
  </head>
</html>
```

# Servlet-API
Wird in einem Servlet die Methode `Servlet.init(javax.servlet.ServletConfig config)` überschrieben ohne `super.init(config)` in der Methode zu rufen, führt dies dazu, dass `getServletConfig()` `null` als Ergebnis liefert und damit zu NullPointerExceptions.
In den Stacktraces ist unter anderem folgende Meldung protokolliert:
```
java.lang.NullPointerException
  at javax.servlet.GenericServlet.getServletContext(GenericServlet.java:123)
```

## Multipart
### Konfiguration notwendig
Zur Nutzung von Multipart-Formularen ist die Multipart-Annotation notwendig wie im folgenden Listing. Sonst kommt es zur Fehlermeldung `No Multipart config found`.
```java
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

@MultipartConfig(location="/tmp", maxFileSize = 10485760L)
public class TestServlet extends HttpServlet{

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)throws IOException, ServletException{
       for(Part part : request.getParts()){
           System.out.println(part.getSubmittedFileName());
       }
    }

}
```

### Parts - Parameter und Dateien
`request.getParts()` liefert sowohl Parameter von HTML input-Elementen als auch Dateien von HTML `<input type="file">`-Elementen. Die Methode `HttpServletRequest.getParameter(String name)` liefert keine Parameter, die dem Multipart-Formular mitgegeben wurden.

# Inkompatible Java-Versionen
Wurde eine Webapplikation mit einer höheren Java-Version kompiliert als auf dem Webserver installiert ist (z.B. für JRE 1.11 kompiliert und Apache Tomcat läuft mit JRE 1.8), startet Apache Tomcat diese Applikation nicht ohne eine diesbezügliche Fehlermeldung zu protokollieren. Beim Aufruf eines Servlets bzw. eines WebSocket-Endpoints gibt Apache Tomcat den HTTP-Statuscode 404 NOT Found zurück.

# Webapp-Konfiguration `web.xml`
## Sicherheitseinschränkungen
Ohne die Angabe einer Berechtigungsrolle wie im folgenden Listing ignoriert Apache Tomcat die Sicherheitseinschränkungen. Es erfolgt keine Benutzer und Passwordabfrage und `request.getUserPrinicpal() == null`.
```xml
<security-constraint>
  <web-resource-collection>
    <url-pattern>/path/to/resource</url-pattern>
  </web-resource-collection>
</security-constraint>
```
Es sollte immer eine Berechtigungsrolle angegeben werden.
```xml
<security-constraint>
  <web-resource-collection>
    <url-pattern>/path/to/resource</url-pattern>
  </web-resource-collection>
  <auth-constraint>
    <role-name>manager</role-name>
    <role-name>manager-gui</role-name>
  </auth-constraint>
</security-constraint>
```

# Servlets
## Datumsparameter
`request.getParameter("date")` (`request` ist Instanz der Klasse `javax.servlets.http.HttpServletRequest`) liefert das aktuelle Datum auch wenn der Request-Body einen Parameter mit Namen `date` mit einem anderem Datum enthält.

# Cache
Per Default cachen die Browser statische Resourcen (JS, CSS, Bilder), die per Apache Tomcat ausgeliefert werden. Der Cache hat kein Ablaufdatum. Wird die Webapplikationen erneuert, bekommt dies der Browsercache unter Umständen nicht mit. Abhilfe schafft unter anderem ein [Cache-Filter](https://github.com/samaxes/javaee-cache-filter). Die Datei `web.xml` muss dazu folgenden Filter enthalten:
```xml
  <filter>
    <filter-name>StaticCache</filter-name>
    <filter-class>com.samaxes.filter.CacheFilter</filter-class>
    <init-param>
      <param-name>expiration</param-name>
      <param-value>3600</param-value>
    </init-param>
    <init-param>
      <param-name>must-revalidate</param-name>
      <param-value>true</param-value>
    </init-param>
    <init-param>
      <param-name>private</param-name>
      <param-value>true</param-value>
    </init-param>
  </filter>
  <filter-mapping>
    <filter-name>StaticCache</filter-name>
    <url-pattern>*.css</url-pattern>
  </filter-mapping>
  <filter-mapping>
    <filter-name>StaticCache</filter-name>
    <url-pattern>*.js</url-pattern>
  </filter-mapping>
```
Alternativ ist es möglich die Dateinamen umzubenennen und damit den Cache zu umgehen.
