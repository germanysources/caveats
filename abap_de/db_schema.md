# Umgebung
- SAP ECC Basis Release 7.40

# Hinweise zum Datenbank-Schema

## MCHA

- Gefüllt wenn Chargenebene eindeutig auf Werks/Materialebene (Funktionsbaustein `VB_BATCH_DEFINITION` gibt `0` im Parameter `KZDCH` zurück)
- `CUOBJ_BM` wird nicht gefüllt, wenn Klassifizierung über `BAPI_OBJCL_CREATE` erstellt wird, nur gefüllt wenn Charge über `VB_CREATE_BATCH` erstellt wird (`KZCLA<>SPACE`)
- Lieferantennummer wird bei Erstellen der Charge:
* aus der Bestellung, wenn ein Wareneingang zur Bestellung gebucht wird,
* aus der Ursprungscharge bei Umlagerungen oder
* aus Customer-Exit `EXIT_SAPMM07M_003`
gelesen.
Eine neue Buchung ändert die Lieferantennr. einer bestehenden Charge nicht.

- Klassifizierungsschlüssel setzt sich Materialnummer, Werk und Chargennummer zusammen (`matnr && werks && charg`)
- Interne Chargennummer kann für Wareneingänge Bestellungen über Customer-Exit `EXIT_SAPLV01Z_002` ermittelt werden oder für Wareneingänge Aufträge aus dem Auftrag stammen
- Aus dem Fertigungssteuerprofil stammt die Einstellung, ob die Charge bei Auftragseröffnung oder -freigabe automatisch oder händisch erstellt wird
- Charge wird durch folgende Aktionen erstellt:
* Materialbuchung (Wareneingang, Umbuchung)
* Erstellen/Freigabe Prozess- bzw. Fertigungsauftrag
* Händisch in Transaktion MSC1N
- `LWEDT`: wird durch Wareneingang (Bewegungsart 101) oder durch Umbuchung (Bewegungsarten 301, 309) gefüllt, wenn bei der Umbuchung der Datensatz in der Tabelle MCHA erstellt wird.

# MCH1

- Gefüllt wenn Chargenebene auf Materialebene (Funktionsbaustein `VB_BATCH_DEFINITION` gibt `1` oder `2` im Parameter `KZDCH` zurück)
- Klassifizierungsschlüssel setzt sich Materialnr. und Chargennr. zusammen

# MSEG

- Eine Belegstornierung erzeugt einen neuen Materialbeleg.
Bsp.: Stornierung des Beleges mblnr=1, mjahr=2021, zeile=1 erzeugt den Materialbeleg mblnr=2, mjahr=2021, zeile=1, smbln = 1, sjahr=2021, smblp=1 in der Tabelle `MSEG`
- Soll/Haben-Kennzeichen: wird in Bewegungsart festgelegt

# AUFM

- Wird nur gefüllt, wenn Kennzeichen `MBWAP` (geplante Warenausgänge), `MBWAU` (ungeplante Warenausgänge) oder `MBWEF` (Wareneingang zum Fertigungsauftrag) in Tabelle `T399X` gesetzt sind.

# AFRU

- `ISM01` (Rückgemeldete Zeiten) nicht gesetzt, wenn Satzartgruppe für Dauer im Arbeitsplatz == 'Variable Leistung' anstatt 'Bearbeiten' oder der Kalkulationsreiter im Arbeitsplatz nicht gepflegt wurde bei Rückmeldung von Start- und Ende Bearbeiten über CORZ oder über die Prozessmeldung `PI_PHST`. Die Rückgemeldeten Zeiten fehlen ebenso in der Kostenermittlung des Fertigungs- bzw. Prozessauftrages.
- Stornierung Rückmeldung: Rückmeldung bekommt Stornokennzeichen (`STOKZ`) und eine neue Rückmeldung wird erzeugt mit Rückmeldzähler (`STZHL`) der stornierten Rückmeldung.
- Satzart: Konstanten für Satzart sind in Include `LCORSART`
- Start-Durchführen: Bei einer Start-Bearbeiten-Meldung (Satzart "B10") nach einer Unterbrechung (Satzart "B30") ist der Startzeitpunkt Durchführen der Zeitpunkt der ersten Start-Bearbeiten-Rückmeldung, nicht der Zeitpunkt der aktuellen Meldung

# CRCO (Arbeitsplatzkostenstellenzuordnung)

- Fehlt der Arbeitsplatz in dieser Tabelle, sind die rückgemeldete Zeiten in der Tabelle `AFRU` leer bei Rückmeldung von Start- und Ende Bearbeiten über CORZ oder über die Prozessmeldung `PI_PHST`.

# COSP, COSS, COSL, COSB
- Kostenermittlung Prozess- und Fertigungsaufträge über Funktionsbaustein `K_KKB_KKBCS_CO_OBJECT_READ` lesbar.
- Kosten werden nur für kalkulationsrelevante Vorgänge ermittelt (Feld `AFVC-SELKZ`). Das Kennzeichen "Kalkulationsrelevant" wird aus dem Planungsrezept geerbt.

# AFPO
- KDAUF, KDPOS, KDEIN sind nur gefüllt, wenn Aufträge mit Bezug zum Kundenauftrag erstellt werden (in Transaktion VA02). Die Auftragsnummer wird zudem in die Tabelle VBEP geschrieben.

# AFKO
- `GETRI` und `GEUZI` (Ist-Ende-Termin) wird nur gefüllt, wenn alle Phasen zu den relevanten Vorgängen rückgemeldet sind. Phasen, die bei der Rückmeldung übersprungen werden, erhalten eine L20-Rückmeldung, wenn eine Anordnungsbeziehung im Rezept definiert ist.
- Relevante Vorgänge legt der Steuerschlüssel fest (Tabelle `T430`). Nicht relevant sind Vorgänge, bei denen eine Rückmeldung vorgesehen aber nicht notwendig ist. 

# AFVC
- Enthält für jede Phase die Sekundärresourcen als zusätzliche Zeile. Bei Sekundärresourcen ist das Feld `PVZKN` initial.
- Vorgänge und Phasen bekommen bei Prozessaufträgen keine Löschvormerkung (Tabelle `JEST` - Status `I0076`), wenn im Prozessauftrag eine Löschvormerkung gesetzt ist.

# AFVV
- Enthält die terminierten Start- und Endezeiten sowie die Leistungsvorgaben der Auftragsphasen.
- Die Felder `FSAVD`, `FSAVZ`, `FSSBD`, `FSSBZ`, `FSSAD`,`FSSAZ` werden bei Neuterminierung des Auftrages mit den rückgemeldeten Zeiten überschrieben, sofern eine Rückmeldung vorhanden ist.

# KNA1

- Zwischen Kunde und Lieferant ist nur eine eins zu eins Zuordnung möglich. Die Änderung der Kreditorennr. in den Debitorenstammdaten bewirkt in der Tabelle LFA1, dass nur ein Kreditor dem Debitor zugewiesen ist.
Bsp vor Änderung `LFA1`:
LIFNR | KUNNR
------|------
100   | 3
101   | 3
`KNA1`:
KUNNR | LIFNR
------|------
3     | 101
Wird nur der Debitorstamm des Debitors `3` (Kreditor von `101` nach `100`) geändert, hat dies folgendes Resultat in der Tabelle `LFA1` zur Folge:
LIFNR | KUNNR
100   | 3
101   |

# EKPO

- `LOEKZ` kann die Werte 'S' für gesperrt oder 'L' für gelöscht besitzen. Gesperrte Positionen schlagen trotzdem noch in der Materialbedarfsplanung auf.
- Kontierung Kostenstelle: Bei Buchung Wareneingang wird keine Charge erstellt, auch wenn das Material (`EKPO-MATNR`) chargenpflichtig ist.
- Bewertungsart `BWTAR`: Wird bei Buchung des ersten Wareneingangs gefüllt, wenn die wareneingangsbezogene Rechnungsprüfung nicht aktiv ist. Wenn die Bewertungsart gefüllt ist, wird die Bewertungsart und die Chargenrnr. des ersten Wareneingangs bei Buchung des zweiten Wareneingangs in der Transaktion MIGO übernommen.
- Wareneingangsbezogene Rechnungsprüfung: Das Kennzeichen stammt aus dem Einkaufsinfosatz oder aus dem Kreditor.

# EKKN

- Datensätze werden nur für kontierte Bestellungen erzeugt. Für nicht kontierte Bestellungen (`EKPO-KNTTP` ist initial) existiert kein Datensatz.
- `NETWR` ist für Kontierung Kostenstelle und Anlage leer.

# EKBE

- Wird unter anderem bei Warenbewegungen auf Bestellungen (Bewegungsarten 101, 643) gefüllt (`VGABE=1`, `VGABE=6`). 
- Wareneingang: Funktionsbaustein `BAPI_GOODSMVT_CREATE` füllt `ETENS` immer mit der ersten Bestellbestätigung (Eine Buchung über migo dagegen mit der aktuellen Bestellbestätigung) siehe Hinweis [906314](https://me.sap.com/notes/906314).
- Rechnungseingang: füllt die Felder `ETENS` (laufende Nummer Bestellbestätigung) und `CHARG` (Chargennummer) nicht.
- Bezugsnebenkosten werden in der Tabelle `EKBZ` festgehalten nicht in der Tabelle `EKBE`. D. h. ein Wareneingang mit Bezugsnebenkosten erzeugt einen Datensatz in der Tabelle `EKBE` und einen Datensatz in der Tabelle `EKBZ`. Der Wert in der Tabelle `EKBE` (Felder `DMBTR` und `WRBTR`) für einen Wareneingang enthält keine Nebenkosten. Eine Rechnung erzeugt entweder einen Datensatz in der Tabelle `EKBZ`, wenn sie nur Nebenkostenpositionen enthält, oder einen Datensatz in der Tabelle `EKBE`, wenn sie nur Waren/Dienstleistungspositionen enthält, oder einen Datensatz in der Tabelle `EKBE` und einen Datensatz in der Tabelle `EKBZ`, wenn sie Waren/Dienstleistungs- und Nebenkostenpositionen enthält.
- Feld `BWTAR` (Bewertungsart): wird für Wareneingänge und Rechnungen gefüllt.
- Feld `AREWR` (WE/RE Ausgleichswert): wird bei Rechnungen und Bestellungen ohne Kontierung mit dem Wareneingangswert gefüllt. Preisdifferenzen haben keinen Einfluss auf diesen Wert. Bei Rechnungen mit Kontierung ist der Wert leer.
- Felder `DMBTR` (Betrag Hauswährung) und `WRBTR` (Betrag) sind bei Kontopflegebelegen leer.
- Unter den Vorgang "Rechnungseingang" (`VGABE='2'`) fallen auch Kontopflegebelege, die in der Transaktion MR11 erstellt wurden.

# EKBZ

- Feld `BWTAR` (Bewertungsart): wird für Wareneingänge und Rechnungen gefüllt.
- Feld `AREWR` (WE/RE Ausgleichswert): wird bei Rechnungen mit dem Wareneingangswert gefüllt. Preisdifferenzen haben keinen Einfluss auf diesen Wert.
- Feld `LIFNR` (Kreditor): Kann bei Bezugsnebenkosten unterschiedlich zum Lieferant aus dem Einkaufsbeleg sein. Stammt aus dem Kreditor, der in den Konditionen eingegeben wurde, bzw. aus dem Kreditor, der bei Buchung Rechnungseingang Bezugsnebenkosten verwendet wurde.
- Felder `DMBTR` (Betrag Hauswährung) und `WRBTR` (Betrag) sind bei Kontopflegebelegen leer.
- Unter den Vorgang "Rechnungseingang" (`VGABE='2'`) fallen auch Kontopflegebelege, die in der Transaktion MR11 erstellt wurden.


# EKET

- Feld `WEMNG`: Die Wareneingangsmenge wird immer gefüllt, auch wenn mit Bestätigungen gearbeitet wird.

# EKES
Bestellbestätigungen, auf die ein Wareneingang gebucht werden kann.

- Wenn alle Einteilungen vollständig beliefert wurden, werden die offenen Bestellbestätigungen noch in der Materialbedarfsplanung übernommen.
- Wareneingang: Funktionsbaustein `BAPI_GOODSMVT_CREATE` schreibt die abgebaute Menge (Feld `DABMG`) in die erste Einteilung.

# T147K
Lieferantenbeurteilung Zuordnung Prozentwerte Treuekriterien und Punktwerte Noten

- Feld `KRPRZ`: Die Werte werden mit der ABAP-Anweisung UNPACK erzeugt. Vor Abfragen sollten Hostvariablen immer Werte mittels der ABAP-Anweisung UNPACK zugewiesen werden:
```abap
DATA: percent_variance(2) TYPE p DECIMALS 1 VALUE '5',
      unpacked_variance TYPE t147k-krprz,

DATA(sign) = COND t147k-krvrz( WHEN percent_variance >= 0 THEN '+' ELSE '-' ).
UNPACK percent_variance TO unpacked_variance.
SELECT * FROM t147k WHERE ekorg = @ekorg AND krtyp = @krtyp AND krprz = @unpacked_variance
   AND krvrz = @sign.
```

# ELBP
Kriterien, die händisch gefüllt werden, und für die keine Gewichtung in der Tabelle `T147C` gepflegt ist, werden mit dem Report `RM06LBAT` initialisiert.

# LIKP

- Vorgängerbeleg kann Kundenauftrag oder Umlagerungsbestellung sein
- Lieferschein hat keinen Bezug zur Einteilung im Kundenauftrag. Beim Erstellen wird das Lieferdatum mit der nächsten offenen Einteilung vorbelegt. Eine Änderung des Lieferdatums wirkt sich nicht auf den Kundenauftrag aus.
- Felder `BTGEW` (Bruttogewicht) und `NTGEW` (Nettogewicht) sind bei Chargensplit, der im User-Exit `USEREXIT_BATCH_DETERMINATION` (`SAPMV50A`) erzeugt wird, initial. (Todo Untersuchen)
- Löschen des Lieferscheins in der Transaktion VL02N löscht den Datensatz in der Tabelle `LIKP` sowie die Positionen in der Tabelle `LIPS`


# KONV

- Konditionen für Vertriebsbelege und Einkaufsbelege. Der Konditionsschlüssel stammt aus dem Beleg (Tabelle VBAK Vertriebsbelege, EKKO Einkaufsbelege), die Positionsnummer stimmt mit der Positionsnummer des Beleges überein
- Existiert nur ECC, in S/4 HANA wurde die Tabelle durch den View `V_KONV` ersetzt [2220005](https://me.sap.com/notes/2220005).

# KONP

- Konditionsstammdaten (Transaktionen VK11, VK12, VK13, MEK32), die in die Belege übernommen werden. Über den Konditionsschlüssel (KNUMH) sind die Konditionen mit den entsprechenden Konditionstabellen verknüpft.

# VBAK

- `KKBER` ist leer, wenn keine Kreditstammdaten in Tabelle `KNKK` für den Auftraggeber vorhanden sind.

# VBEP
Einteilungen sind bei Verkaufsbelegenpositionen, die nicht lieferrelevant sind und bei denen keine Einteilungen erlaubt sind, nicht vorhanden (Dies betrifft beispielsweise Gut- und Lastschriftsanforderungen).

# BSEG
- Steuerbeträge in den Feldern `MWSTS` und `WMWST` sind nur gefüllt, wenn der Steuerbetrag manuell eingegeben wurde. Setzt der Sachbearbeiter das Kennzeichen "Steuer automatisch berechnen", bleiben diese Felder leer (siehe Hinweis [2528581](https://me.sap.com/notes/0002528581));

# Bestände

## MBEW

- Datensatz mit leerer Bewertungsart wird im Materialstamm gepflegt.
- Fehlende Wertfortschreibung für Materialien (Tabelle `T134M`): Gesamtbestand und Gesamtwert sind null.
- Für Kundenauftragsbestand (Sonderbestandskennzeichen 'E') wird kein Bestand in dieser Tabelle geführt.

## MBEWH

- Wird nur bei Bestandsveränderungen befüllt (siehe Dokumentation zum Element `VMLAB`)

## MCHB

- Chargenbestände für Chargen auf Werks/Materialebene
- Sonderbestände (Kundenauftragsbestand, Konsignationsbestand ...) sind nicht in dieser Tabelle enthalten.
- Vorperiodenbestände seit Release 4.5 nicht mehr gefüllt (siehe Dokumentation zum Element `VMLAB`).

## MCHBH

- Wird nur bei Bestandsveränderungen befüllt (siehe Dokumentation zum Element `VMLAB`)

## MARD

- Bestände werden für Chargen- sowie für nicht Chargengeführte Materialien fortgeschrieben. 
- Fehlende Mengenfortschreibung (Tabelle `T134M`): Bestand ist null.

## MSKA

- Kundenauftragsbestand: bei Buchung Wareneingang auf Fertigungsauftrag mit Bezug zum Kundenauftrag wird der Bestand hier fortgeschrieben

## MSKU

- Enthält unter anderem Kundenkonsignationsbestände (Sonderbestandskennzeichen 'W')

# MKAL (Fertigungsversionen)

- Feinplanung (Verwendung in Prozessaufträgen): Felder `PLNTY`, `PLNNR`, `ALNAL` enthalten das Planungsrezept
- Grobplanung: Felder `PLTYG`, `PLNNG`, `ALNAG`
- Ratenplanung: Felder `PLTYM`, `PLNM`, `ALNAM`

# MDVM 
Planungsvormerkungen für Materialbedarfsplanung. Aufbau erfolgt im Transaktion `OMDO`. Ohne Aufbau werden keine Planaufträge für Vorplanbedarfe erstellt.

# NRIV
Nummernkreisintervalle sowohl fuer jahresabhängige als auch für jahresunabhängige Nummernkreise. Die Selektion der Nummernkreiseintervalle befindet sich im Funktionsbaustein `NUMBER_GET_INFO`. Bei Jahresabhängigen Nummernkreisen wird mit größer als "toyear" selektiert.

# T001F
Formulare fuer Finanzbuchhaltung.

# PCL2

## Cluster "B2"
### Arbeitsplan in Tabelle PSP
- Arbeitsplan in Tabelle PSP und Sollzeiten in Tabelle ZES (Zeitart "0002") müssen nicht übereinstimmen.
- Die Sollarbeitsstunden eines Arbeitstages sind teilweise in mehreren Perioden vorhanden. Die Sollarbeitsstunden vom 31.03 sind beispielsweise in den Abrechnungsperioden März und April vorhanden.

### Abwesenheiten in Tabelle AB
Die Abwesenheitstage und -stunden (Tabelle AB) werden aus der Sollzeit (Arbeitsplan) entnommen.
	Bsp.:
	Arbeitsplan:
	04.01.2021	8 h
	05.01.2021	8 h
	06.01.2021	0 h
	Abwesenheit vom 04.01 - 06.01: 16 h Abwesenheit, 2 Tage Abwesenheit
### Abwesenheiten in Tabelle AB über mehrere Abrechnungsperioden
Diese sind in mehreren Abrechnungsperioden vorhanden, auch wenn die Abwesenheit nicht innerhalb der Abrechnungsperiode liegt.
	Bsp.: Eine Abwesenheit von 01.03. bis 03.03 kann noch in der Abrechnungsperiode Februar vorkommen.
	BEGDA und ENDDA: Diese Werte können Abrechnungsperiodenübergreifend sein.
	Bsp.: Abwesenheit von 15.02 bis 10.4
		BEGDA ENDDA
	Feb.	15.02	10.04
	Mar.	15.02	10.04
	Apr.	15.02	10.04

## Cluster "CU"
Cluster "CU" enthält die Abrechnungen:
```abap
DATA: employee_no TYPE p_pernr.

IMPORT rgdir = payroll_sequences FROM DATABASE pcl2(cu) ID employee_no.
LOOP AT payroll_sequences REFERENCE INTO DATA(sequence)
    WHERE fpper IN payroll_periods.
  DATA(rd_cluster_key) = VALUE pc200( pernr = employee_no seqno = sequence->*-seqnr ).
  IMPORT rt = salaries FROM DATABASE pcl2(rd) ID rd_cluster_key.
  LOOP AT salaries REFERENCE INTO DATA(salary).
  ENDLOOP.
ENDLOOP.
```
