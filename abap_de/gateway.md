# Stolperfallen bei der Arbeit mit dem SAP Gateway

## Datumsfelder
Initiale Datumsfelder führen zu einem Fehler beim Abruf der Entitätsmenge, wenn die entsprechende Entitätseigenschaft keine Nullwerte annehmen darf.

## Große Ergebnismengen
Große Ergebnismengen können Timeouts bzw. Laufzeitfehler `STRING_SIZE_TOO_LARGE` oder `SYSTEM_NO_ROLL` zur Konsequenz haben. `STRING_SIZE_TOO_LARGE` und `SYSTEM_NO_ROLL` treten auf, wenn nicht genügend erweiterbarer - bzw. Heap-Speicher dem Workprozess zur Verfügung steht.
### Vermeidungsstrategien
Wenn eine Einschränkung der Ergebnismenge nicht durch Filterung erreicht werden kann:
- Performante SQL-Anweisungen wählen (`FOR ALL ENTRIES IN` ist bei entsprechend vielen Werten nicht sehr performant)
- Minimale Ausgabe (nur Angabe der benötigten Felder im $select-Parameter)

## Schreibzugriffe
### CSRF-Token
Bei [Schreibzugriffen](https://blogs.sap.com/2021/11/04/csrf-token-validation-failed-in-post-method-in-gateway-client/) ist ein CSRF-Token im HTTP-Header `x-csrf-token` erforderlich. Dieses kann per HTTP-GET-Request mit dem Header `x-csrf-token: fetch` gelesen werden.
```HTTP
GET /sap/opu/odata/sap/ZSERVICE/EntitySet
x-csrf-token: fetch

HTTP/1.1 200 OK
x-csrf-token: '12345'
set-cookie: _ga=...
```

```HTTP
POST /sap/opu/odata/sap/ZSERVICE/EntitySet
x-csrf-token: '12345'
Content-Type: application/json
Cookie: _ga=...

{
  "id": "400",
  ...
}
```

Das CSRF-Token hat eine zeitliche Gültigkeit.

### Dezimalzahlen in OData v2
Dezimalzahlen sind im JSON-Format als Strings ausgeführt. Im JSON-Format dürfen diese in POST-Requests nicht im numerischen Format angegeben werden.
Negativbeispiel:
```JSON
{
  "value": 10.5
}
```
Positivbeispiel:
```JSON
{
  "value": "10.5"
}
```

## Dynamische Selektionslisten
Bei Verwendung von Fluchtsymbolen `@` vor Hostvariablen (Open SQL-Anweisungen) greift der strikte Modus. Die folgenden Anweisungen führen im strikten Modus zu dem Fehler `Der Parser lieferte den Fehler: Die Elemente der "SELECT LIST"-Liste müssen mit Kommata getrennt werden.`, da die Methode `io_tech_request_context->get_select_with_mandtry_fields( )` die Felder ohne Kommata übergibt.
```abap
DATA(selected_fields) = io_tech_request_context->get_select_with_mandtry_fields( ).
IF selected_fields IS INITIAL.
  INSERT CONV string( '*' ) INTO TABLE selected_fields.
ENDIF.

DATA(osql_where_clause) = io_tech_request_context->get_osql_where_clause( ).
SELECT (selected_fields) FROM spfli
  WHERE (osql_where_clause)
  INTO CORRESPONDING FIELDS of TABLE @et_entityset.
```

Die selektierten Felder benötigen nach dem Namen jeweils ein Komma wie im folgenden Listing, um den Fehler zu vermeiden.
```abap
DATA(selected_fields) = io_tech_request_context->get_select_with_mandtry_fields( ).
IF selected_fields IS INITIAL.
  INSERT CONV string( '*' ) INTO TABLE selected_fields.
ELSE.
  LOOP AT selected_fields ASSIGNING FIELD-SYMBOL(<field>)
      FROM 1 TO lines( selected_fields ) - 1.
    <field> = |{ <field> },|.
  ENDLOOP.
ENDIF.

DATA(osql_where_clause) = io_tech_request_context->get_osql_where_clause( ).
SELECT (selected_fields) FROM spfli
  WHERE (osql_where_clause)
  INTO CORRESPONDING FIELDS OF TABLE @et_entityset.
```

## Soft-Cancel
Beim clientseitigen Abbruch der Verbindung wie im folgenden Listing erscheint im Fehlerprotokoll (Transaktion `/IWFND/ERROR_LOG´) keine Fehlermeldung. Ebenso wird kein Laufzeitfehler erstellt.
```js
const abortController = new AbortController();

fetch(`/sap/opu/odata/sap/ZSERVICE/EntitySet`, {
  signal: abortController.signal
})
  .then(response => response.text().then(console.log))
  .catch(console.error);
abortController.abort();
```

In den Fehlerprotokolldateien (Transaktion `ST11`) ist unter Umständen folgende Fehlermeldung zu sehen:
```
 -->DbSlControl(con=0,cmd=57=DBSL_CMD_SQLBREAK)
 C  -->oci_break(con=0,Acon=0,cb=7FF73E66F670): cancel of current SQL
 C     Calling OCIBreak() while 'OCIStmtFetch2()' (Acon=0) is running
 C     OCIBreak() -> 0
 C  <--oci_break() -> Aorc=0
 C  <--DbSlControl(con=0,cmd=57=DBSL_CMD_SQLBREAK), rc=0=DBSL_ERR_OK
 B  db_sqlbreak() = 0
 M  program canceled
 M    reason   = soft cancel
 M    user     = 
 M    client   = 
 M    terminal = 
 M    T67_U17877_M0
 M    report   = 
 C  e: OCIBreak(con=0)->0 called for OCIStmtFetch2()->0
 C  e: OCIBreak(con=0)->0 -> 'user requested cancel'
 C  SQL cancelled; calling registered fct. Cbcancel at 00007FF73E66F670
 M  program canceled
 M    reason   = soft cancel
 M    user     = 
 M    client   = 
 M    terminal = 
 M    T67_U17877_M0
 M    report   = 
 B  db_con_test_and_open: bad cursor delta, reset open cursors to 0
 M  ThPerformDeleteThisSession: switch off statistics
```

Anwendungen, die einen clientseitigen Abbruch erzeugen, sind unter anderem das Microsoft PowerBI On-Premise Gateway. Das OnPremise-Gateway erstellt beim Abbruch folgende Fehlermeldung:
```json
{
  "error":
    {
      "code":"DM_GWPipeline_Gateway_MashupDataAccessError",
      "pbi.error": { 
        "code":"DM_GWPipeline_Gateway_MashupDataAccessError",
	"parameters":{},
	"details":[
	  {
	    "code":"DM_ErrorDetailNameCode_UnderlyingErrorCode",
	    "detail":{
	      "type":1,
	      "value":"-2147467259"
	    }
	  },
	  {
	    "code":"DM_ErrorDetailNameCode_UnderlyingErrorMessage",
	    "detail":{
	      "type":1,
	      "value":"Von der Übertragungsverbindung können keine Daten gelesen werden: Die Verbindung wurde geschlossen."
	    }
	  },
	  {
	    "code":"DM_ErrorDetailNameCode_UnderlyingHResult",
	    "detail":{
	      "type":1,
	      "value":"-2147467259"
	    }
	  },
	  {
	    "code":"Microsoft.Data.Mashup.ValueError.DataSourceKind",
	    "detail":{
	      "type":1,
	      "value":"OData"
	    }
	  },
	  {
	    "code":"Microsoft.Data.Mashup.ValueError.DataSourcePath",
	    "detail":{
	      "type":1,
	      "value":"..."
	    }
	  },
	  {
	    "code":"Microsoft.Data.Mashup.ValueError.Reason",
	    "detail":{
	      "type":1,
	      "value":"DataSource.Error"
	    }
	  }
	],
      "exceptionCulprit":1
    }
  }
}
```
