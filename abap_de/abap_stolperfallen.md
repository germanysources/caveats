# Stolperfallen in ABAP

## obsulete Sprachelemente
- `DELETE FROM db_table`: hier erfolgt keine Mandantenbehandlung, fehlt der Mandant werden alle Datens�tze gel�scht!
   
- Gruppenstufenverarbeitung
```abap
LOOP AT itab.
  AT END of (name).
    " Alle Zeichenartige Elemente hinter name sind ausgesternt!
  ENDAT.
ENDLOOP.
```

## Datenbankbefehle
- `SELECT (list) FOR ALL ENTRIES IN`: Doppelt vorkommende Zeilen werden aus der Ergebnismenge entfernt. Deshalb sollte der Schl�ssel immer vollst�ndig in der Selektionsliste angegeben werden. Folgende Anweisung entfernt Lieferscheine mit gleicher Menge:
```abap
SELECT werks, menge FROM lips
  FOR ALL ENTRIES IN @delivery_notes
  WHERE vbeln = @delivery_notes-vbeln
  INTO TABLE @DATA(delivery_quantities).
```
Damit dies nicht mehr passiert, muss die Selektionsliste den Prim�rschl�ssel der Tabelle `LIPS` enthalten:
```abap
SELECT vbeln, posnr, werks, menge FROM lips
  FOR ALL ENTRIES IN @delivery_notes
  WHERE vbeln = @delivery_notes-vbeln
  INTO TABLE @DATA(delivery_quantities).
```

- SELECT COUNT
```abap
DATA count type i
SELECT COUNT(*) INTO count FROM (table).
```
Hier kommt es zu dem Laufzeitfehler `DBSQL_CONVERSION_ERROR`, wenn zu viele Eintraege vorhanden sind! Daher sollte hier die Ausnahme `cx_sy_open_sql_db` abgefangen werden bzw. muss die Variable `count` entsprechend gro� dimensioniert sein! 
  
- Joins
Die Where-Bedingung wirkt auf beide Seiten des Joins:
```abap
SELECT * FROM tab1 LEFT OUTER JOIN tab2 ON tab2~key = tab1~key
  WHERE tab2-field = 'ABC'.
```
Ist die Zeile in `tab2` nicht vorhanden, wird die Zeile auch nicht gelesen trotz des outer joins! Das Ergebnis ist dasselbe wie beim inner join!
```abap
SELECT * FROM tab1 LEFT OUTER JOIN tab2 ON tab2~key = tab1~key
  INTO CORRESPONDING FIELDS OF itab.
```
Kommt ein Feld sowohl in `tab1` als auch in `tab2` vor und in `tab2` existiert der Datensatz nicht, hat dieses Feld Initialwert!

- SELECT SINGLE
`SELECT SINGLE` initialisiert die ABAP-Variablen vorher nicht.

- Strikter Modus (Strict mode) Kernel-Release 745: Im strikten Modus kommt es zum Laufzeitfehler `SAPSQL_DATA_LOSS`, wenn ein Literal l�nger ist als das entsprechende ABAP-Feld.
```abap
SELECT * FROM t100 WHERE sprsl = 'ES' INTO @DATA(messages).
```
Dies trifft auch f�r zu gro� dimensionierte Selektionsoptionen zu:
```abap
TYPES: _range TYPE RANGE OF char100.
DATA(languages) = VALUE _range( ( sign = 'I' option = 'EQ' low = 'ES' ) ).
SELECT * FROM t100 where sprsl IN @languages.
```
Eine Mischung aus f�hrenden Leerzeichen bei Character-Feldern sorgt f�r eine irref�hrende Sortierung auf der Datenbank. 
Tabelle `ZBOOK`:

CARRID | BOOKING_ID (CHAR 3)
------ | -------------------
'AA'   | '1'
------ | -------------------
'AA'   | ' 2'
------ | -------------------
'AA'   | ' 3'

```abap
SELECT max( booking_id ) FROM zbook WHERE carrid = 'AA'
  INTO @DATA(last_booking_id).
WRITE: last_booking_id.
```
gibt '1' anstatt '3' aus wegen der f�hrenden Leerzeichen. F�hrende Leerzeichen k�nnen entstehen, wenn Integer-Felder an Character-Felder zugewiesen werden.

## Packed numbers
Wird eine Datenbanktabelle mit gepackte Nummern mit geraden Stellenanzahl im Dictionary definiert, sind die entsprechenden ABAP-Felder eine Stelle l�nger. Beim Einf�gen kann dies unter Oracle den Fehler `ORA-01438 value larger than specified precision allowed for this column` zur Folge. Gepackte Nummern sollten im Dictionary daher immer mit einer geraden Stellenanzahl definiert sein.

### Selektionstabellen
Gro�e Selektionstabellen f�hren zur Ausnahme `CX_SY_OPEN_SQL_DB` (Die Anweisung, die an die Datenbank gereicht werden sollte, ist zu umfangreich).

## Hintergrundjobs
Ausgeblende Parameter in der Variantensteuerung haben beim Aufruf des Programms Initial-Wert.

## Objekte
- Bei dem Down-Cast zwischen Klassen sollte immer gepr�ft werden, ob die Ausnahme `cx_sy_move_cast_error` auftritt. Dies kann vorkommen, wenn in der Subklasse neue statische Attribute definiert wurden.
- Wird eine Klasse erweitert (z.B. neue Methode), k�nnen in den Subklassen trotzdem Syntaxfehler auftreten, wenn eine Methode mit gleichem Namen in der Subklasse definiert wurde.
- Beim Up-Cast bleiben redifinierte Methoden erhalten!

## Redefinition
Wird eine `super->method( )` gerufen, die Methodenaufrufe der Superklasse enth�lt, wird die Methodenimplementation der redefinierten Klasse gezogen.

## Verbuchung
Verbuchungsroutinen (auch wenn diese nicht mit call function ... in update task registriert werden ) sollten moeglichst frei von GUI Elementen sein. Durch Dynpro-Wechsel wird hier ein impliziten Commit Work ausgel�st. Dies ist meistens nicht gew�nscht.
Bei Verwendung von Verbuchungsbausteinen sollte immer ein explizites `COMMIT WORK` folgen, sonst bleiben Orphans in Tabelle VBDATA wie Hinweis 1223994 beschrieben stehen.

## Uncommitted Read
Nur auf der Oracle und SAP HANA Datenbank gibt es denn committed read.
Bei einem committed read werden die Daten erst gelesen, wenn diese mit commit work fortgeschrieben wurden.
Uncommitted read heisst, sobald eine Anweisung UPDATE, INSERT, MODIFY ausgef�hrt wird, wird dieser Satz gelesen. Bei den anderen Datenbanken k�nnen auch Daten gelesen werden, die durch einen rollback work wieder ger�ckg�ngig gemacht werden k�nnen.

## Mehrfache UPDATE Anweisungen
Ein mehrfache Ausf�hrung einer UPDATE-Anweisung auf die gleiche Tabellenzeile in einer Datenbank-LUW ist undefiniert!
Parellele MODIFY Anweisungen warten darauf, bis die andere beendet wurde. Die Sperre wird auf der Datenbank gehalten bis zum n�chsten commit work. Siehe Hinweis 84348. In der Transaktion st04 sind die Sperren ersichtlich.

## Programmeigenschaften
Festpunktarithmetik bedeutet, Gleitkommazahlen und Dezimalzahlen (packed numbers) werden auch als solche behandelt.
Sonst werden diese bei Berechnungen als Integer behandlet. D.h. ein Vergleich 0.55 < 0.666 wird in 55 < 666 umgewandelt

## Optionale Parameter
Die Bedingung IS SUPPLIED funktioniert nur in der 1. Methode der Aufrufhirachie. 

## Wert�bergabe bei Methoden und Funktionsbausteinen
Werden Ausnahmen ausgel�st, werden Exporting und Changing Parameter mit Wert�bergabe nicht an den Aktualparameter �bergeben.
    
## Tabellentypen

Erweiterungen:
Wird die zugrunde liegende Struktur erweitert, k�nnen alle Programme, die den Tabellentyp verwenden, Syntaxfehler enthalten
```abap
types: table_type type table of zstructur.
data: sample_tab type table_type,
      sample_struct type zstructur.

read table sample_tab into sample_struct index 1.
```
Wird ein Syntaxfehler liefern, wenn die Struktur von `table_type` ge�ndert wird.
```abap
types: table_type type table of zotherwise.
data: sample_tab type table_type,
      sample_struct type zotherwise.

read table sample_tab into sample_struct index 1.
```

## Tabellenschl�ssel
Wird ein neues Feld in ein Tabellenschl�ssel (sorted table oder hashed table) aufgenommen, darf dies nicht mehr �berschrieben werden:

Bsp.: Das Feld belegart wird in den schl�ssel aufgenommen:
```abap
read table belege assigning <beleg> index 1.
<beleg>-belegart = 'GG'.
```
Dies hat den Laufzeitfehler `cx_sy_dyn_call_illegal_type` zur Folge.

Bei folgender Anweisung kommt diese Ausnahme auch, wenn der Schl�ssel nicht ge�ndert wird:
```abap
sample_method( changing beleg = <beleg> ).
```
Also keine Feld-Symbole, die auf sortierte oder Hash-Tabellen verweisen als changing-Aktualparameter �bergeben!
    
## Endlosschleife:
```abap
LOOP AT sorted_table INTO DATA(wa).
 wa-key = 'ABC'.
 INSERT wa INTO TABLE sorted_table.
ENDLOOP. 
```
oder:
```abap
LOOP AT standard_table INTO DATA(wa).
  APPEND ... TO standard_table.
ENDLOOP.
```

## Implizite Konvertierungen
```abap
DATA char1 TYPE c VALUE 'X'.
IF char1 = 1.
```
Schl�gt fehl mit Laufzeitfehler `CONVT_NO_NUMBER`, wohingegen der folgende Vergleich nicht fehlschl�gt:
```abap
DATA char1 TYPE c VALUE '1'.
IF char1 = 1.
```

## Vorzeichen in Dynpros
Negative Werte sorgen f�r Laufzeitfehler `DYNPRO_FIELD_CONVERSION`, wenn das Dynpro-Element kein Vorzeichen besitzt. Die Existenz eines Vorzeichen wird aus der Dom�ne �bernommen.

## Zeichenketten in Iterationsausdruecken:
Wird eine leere Zeichenkette in der INIT-Anweisung deklariert, ist das Resultat eine leere Zeichenkette bzw. eine Zeichenkette mit einem Zeichen:
```abap
DATA: words TYPE TABLE OF string.

sentence = REDUCE string( INIT text = '' FOR <word> IN words NEXT text = text && <word> ).
```
Richtig ist es eine Hilfsvariable zu deklarieren (im Bsp. `_text`):
```abap
DATA: words TYPE TABLE OF string,
     _text TYPE string.

sentence = REDUCE string( INIT text = _text FOR <word> IN words NEXT text = text && <word> ).
```

## Numerische Werte in Iterationsausdr�cken
Bei Bildung der Summe f�hrt die Angabe `sum = 0` in der INIT-Anweisung dazu, dass die einzelnen Komponenten auf Integer-Werte gerundet werden. Folgendes Listing gibt 2 als Summe aus, da 0,5 auf 1 gerundet wird.
```abap
DATA:
  numbers TYPE TABLE OF menge_d.

APPEND '0.5' TO numbers.
APPEND '0.5' TO numbers.

DATA(_sum) = REDUCE menge_d( INIT sum = 0 FOR <n> IN numbers NEXT sum = sum + <n> ).
WRITE _sum.
```

Besser ist es eine Dummy-Konstante zu definieren und die Hilfsvariable `sum` mit der Dummy-Konstanten zu initialisieren. Dann findet kein Auf- oder Abrundung statt.
```abap
CONSTANTS: _dummy TYPE menge_d VALUE IS INITIAL.
DATA:
  numbers TYPE TABLE OF menge_d.

APPEND '0.5' TO numbers.
APPEND '0.5' TO numbers.

DATA(_sum) = REDUCE menge_d( INIT sum = _dummy FOR <n> IN numbers NEXT sum = sum + <n> ).
WRITE _sum.
```

## Iterationsausdruecke
Wird dieselbe Tabelle als Resultat eines Iterationsausdruck und als Iterationsobjekt verwendet, wird diese Tabelle initialisiert.
```abap
DATA index_rows TYPE TABLE OF i.

index_rows = VALUE #( ( 1 ) ( 2 ) ).
index_rows = VALUE #( FOR idx IN index_rows ( idx + 2 ) ).
ASSERT index_rows IS INITIAL.
```

## Dynamisch typisierte Export-Parameter:
Gegeben sei folgende Klasse:
```abap
CLASS dynamic_parameterized_class DEFINITION.

  PUBLIC SECTION.

    CLASS-METHODS dynamic_export_param
      EXPORTING
        result TYPE any table.

ENDCLASS.

CLASS dynamic_parameterized_class IMPLEMENTATION.

  METHOD dynamic_export_param.
    DATA: carriers TYPE STANDARD TABLE OF scarr.

    result = carriers.

  ENDMETHOD.

ENDCLASS.
```
Beim Aufruf der Methode `dynamic_parameterized_class=>dynamic_export_param` muss dem Formalparameter `result` ein passender Aktualparameter zugewiesen werden. Fehlt die Zuweisung, hat dies ein Konvertierungsfehler zur Folge.

## CONVERT TIMESTAMP
`CONVERT TIMESTAMP` erzeugt beim Wechsel von Winter- auf Sommerzeit den Returncode `sy-subrc` 12.
```abap
CONVERT DATE '20220327' TIME '020200' INTO TIME STAMP DATA(ts)
  TIME ZONE sy-zonlo.
WRITE sy-subrc. " -> gibt 12 aus
```

## Unterschied Zuweisung und MOVE-CORRESPONDING bei internen Tabellen
Im Vergleich zu MOVE-CORRESPONDING kopiert eine Zuweisung mit `=` nicht die einzelnen Komponenten, sondern die gesamte Tabellenzeile und f�llt diese in die Zielstruktur bzw. in das Zielelement.
Dies kann bei folgender Konstellation zu Verschiebungen f�hren:
```abap
TYPES: large_range_type TYPE RANGE OF char100,
       small_range_type TYPE RANGE OF char4.
DATA: large_range TYPE large_range_type,
      small_range TYPE small_range_type.

small_range = VALUE #( ( sign = 'I' option = 'BT' low = '4444' high = '5555' ) ).
large_range = small_range.
```
`large_range` enth�lt nach der Zuweisung Zeile 1 mit:
```
sign = 'I'
option = 'BT'
low = '44445555'
high = ''
```

## OPEN DATASET
### Limit offene Dateien
Werden mehr als 100 Dateien pro interner Sitzung ge�ffnet, muss jede Datei unbedingt vorher geschlossen werden mit `CLOSE DATASET`. Sonst f�hrt dies zum Laufzeitfehler `DATASET_TOO_MANY_FILES`.

### Lesen Bin�rdateien
Beim Lesen mit der Anweisung `READ DATASET` ist `sy-subrc` 4, wenn das Zielfeld l�nger als ben�tigt ist. Bei einer Dateil�nge von 520 Bytes, ist `sy-subrc` beim zweiten Schleifendurchlauf 4 und die Variable `line` enth�lt die letzten 20 Bytes + F�llzeichen. Nur wenn das Dateiende erreicht ist, enth�lt die Variable `line` nur noch F�llzeichen. Daher sollte die Schleife nur abgebrochen werden, wenn die Bedingung `sy-subrc <> 0 and line+0(1) = empty_byte` erf�llt ist.
```abap
CONSTANTS: empty_byte(1) TYPE x VALUE IS INITIAL.
DATA:
  file_path TYPE string,
  line(500) TYPE x,
  content   LIKE STANDARD TABLE OF line.

DESCRIBE FIELD line LENGTH DATA(length) IN BYTE MODE.
OPEN DATASET file_path FOR INPUT IN BINARY MODE.
DO.
  READ DATASET file_path INTO line MAXIMUM LENGTH length
    ACTUAL LENGTH DATA(act_length).
  IF sy-subrc <> 0 AND line+0(1) = empty_byte.
    EXIT.
  ENDIF.
  INSERT line INTO TABLE content.
ENDDO.
CLOSE DATASET file_path.
```

## Strukturkomponenten
Die Methode `cl_abap_structdescr=>get_components` liefert die Komponenten der Includes nicht. Nur die Methode `cl_abap_structdescr=>get_included_view` liefert alle Komponenten inklusive der Komponenten der Includes.

## RFC

### Asynchroner RFC
Laufzeitfehler im asynchronen RFC haben keinen Programmabbruch des Aufrufers zur Folge. Das aufrufende Programm l�uft ohne Fehler weiter. Laufzeitfehler sind nur in der Transaktion ST22 sichtbar.
```abap
FUNCTION zfail.

  ASSERT 1 = 0.

ENDFUNCTION.
```
Das Listing bricht aufgrund dieses Verhaltens nicht ab.
```abap
CALL FUNCTION 'ZFAIL'
  STARTING NEW TASK 'TASK_1'.
```

## Bin�rdaten
Interne Tabellen mit Bin�rdaten enthalten am Ende das F�llbyte 0x00. Im folgenden Listing enth�lt die Tabelle `binary_tab` das F�llbyte 0x00.
```abap
DATA: 
  binary_tab TYPE STANDARD TABLE OF w3mime,
  buffer_in TYPE xstring,
  st TYPE string.

  st = 'Hallo Welt'.
  CALL FUNCTION 'SCMS_STRING_TO_XSTRING'
    EXPORTING
      text = st
    IMPORTING
      buffer = buffer_in.

  CALL FUNCTION 'SCMS_XSTRING_TO_BINARY'
    EXPORTING
      buffer = buffer_in
    TABLES
      binary_tab = binary_tab.
```
Bei Konvertierung von bin�ren Tabellen zum Typ `xstring` sollten die F�llbytes entfernt werden durch Angabe der Byteanzahl im Funktionsbaustein `SCMS_BINARY_TO_XSTRING`. Nur so erh�lt man die Originalbin�rdaten wieder.
```abap
DATA: 
  binary_tab TYPE STANDARD TABLE OF w3mime,
  buffer_in TYPE xstring,
  buffer_out TYPE xstring,
  st TYPE string,
  length TYPE i.

  st = 'Hallo Welt'.
  CALL FUNCTION 'SCMS_STRING_TO_XSTRING'
    EXPORTING
      text = st
    IMPORTING
      buffer = buffer_in.

  CALL FUNCTION 'SCMS_XSTRING_TO_BINARY'
    EXPORTING
      buffer = buffer_in
    IMPORTING
      output_length = length
    TABLES
      binary_tab = binary_tab.

  CALL FUNCTION 'SCMS_BINARY_TO_XSTRING'
    EXPORTING
      input_length = length
    IMPORTING
      buffer = buffer_out
    TABLES
      binary_tab = binary_tab.

  ASSERT buffer_in = buffer_out.
```

## `CALL TRANSFORMATION`
### Gro�- und Kleinschreibung
Bei `CALL TRANSFORMATION` m�ssen Objektschl�sselnamen im JSON-Format gro� geschrieben sein.
Negatives Beispiel:
```json
{
  "carriers": [
    {
      "carrid": "LH",
      "carrname": "Lufthansa"
    }
  ]
}
```
Positives Beispiel:
```json
{
  "CARRIERS": [
     {
       "CARRID": "LH",
       "CARRNAME": "Lufthansa"
     }
  ]
```
Das positive Beispiel l�sst sich in eine interne ABAP-Tabelle serialisieren, w�hrend sie beim negativen Beispiel leer bleibt:
```abap
DATA: carriers TYPE scarr_tab,
      json_content TYPE string.

json_content = '{' &&
  '"CARRIERS": [' &&
    '{' &&
      '"CARRID": "LH"' &&
      '"CARRNAME": "Lufthansa"' &&
    '}' &&
'}'.

CALL TRANSFORMATION id
  SOURCE XML json_content
  RESULT carriers = carriers.
cl_demo_output=>display( carriers ).
```

Die folgende Logik wendet die Gro�schreibung auf alle Objektschl�sselnamen an:
```abap
CLASS json_utility DEFINITION.

  PUBLIC SECTION.

    CLASS-METHODS json_to_upper_case
      IMPORTING
        json_content TYPE xstring
      RETURNING
        VALUE(result) TYPE xstring.

ENDCLASS.

CLASS json_utility IMPLEMENTATION.

  METHOD json_to_upper_case.
    DATA(reader) = cl_sxml_string_reader=>create( json_content ).
    DATA(writer) = CAST if_sxml_writer( cl_sxml_string_writer=>create( type = if_sxml=>co_xt_json ) ).
    DO.
      DATA(node) = reader->read_next_node( ).
      IF node IS INITIAL.
        EXIT.
      ENDIF.
      IF node->type = if_sxml_node=>co_nt_element_open.
        DATA(attributes)  = CAST if_sxml_open_element( node )->get_attributes( ).
        LOOP AT attributes ASSIGNING FIELD-SYMBOL(<attribute>).
          IF <attribute>->qname-name = 'name'.
           <attribute>->set_value(
              to_upper( <attribute>->get_value( ) ) ).
          ENDIF.
        ENDLOOP.
      ENDIF.
      writer->write_node( node ).
    ENDDO.
    result = CAST cl_sxml_string_writer( writer )->get_output( ).
  ENDMETHOD.
ENDCLASS.
```

### Gleitkommazahlen
Gleitkommazahlen f�hren wenn Sie mit Mantisse und Exponent angegeben werden (z.B. 1.45E3) bei der JSON-Deserializierung zur Ausnahme `CX_SY_CONVERSION_NO_NUMBER`.
Hinreichend gro�e Zahlen werden mit JavaScript-Methode `JSON.stringify` bzw. mit der Java-Bibliothek `com.jackson.fasterxml.databind` in dieser Form serializiert.
Bei der Java-Bibliothek `com.jackson.fasterxml.databind` sollten Float und Double-Werte mit BigDecimal-Objekten umh�llt werden und bei der Serializierung muss das Feature `SerializationFeature.WRITE_BIGDECIMAL_AS_PLAIN` wie im folgenden Listing aktiviert werden.
```java
new ObjectMapper()
  .writer()
  .with(SerializationFeature.WRITE_BIGDECIMAL_AS_PLAIN)
  .writeValue(response.getOutputStream(), myObject);
```

### Datumswerte
Im JSON-Format k�nnen nur Datumswerte in der Form YYYY-MM-dd geparst werden. Angaben wie in ABAP YYYYMMdd f�hren zum einem Deserialisierungsfehler.

### ST-Transformationen
Bei der ST-Transformationen f�hren Elemente, die im Template nicht angegeben sind, zur Ausnahme vom Typ `CX_ST_MATCH`.

### Bin�rdaten
Im JSON-Format sind Bin�rdaten Base64 codiert.

## GUI-Status-Aktivierung
Wenn zu wenig Platz zur GUI-Status-Aktivierung besteht, kommt es zur Meldung `Storage area for GUI status MENUSYST too small` oder zur Meldung `Workprozess wurde neu gestartet; Backend-Session abgebrochen` in der SAP GUI sowie unter Umst�nden zum Laufzeitfehler `SYSTEM_CORE_DUMPED` (Kernel 753 ist unter anderem betroffen). 
![Workprozess Error](workprozess_error.png)

Abhilfe schafft die Erh�hung des Parameters `ztta/cua_area` (Transaktion RZ11), der die Gr��e des Arbeitsbereiches zur Aktivierung und Generierung einer CUA-Oberfl�che beschr�nkt (siehe Hinweis [2491252](https://me.sap.com/notes/0002491252)).
