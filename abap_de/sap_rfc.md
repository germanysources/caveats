# JCo und NCo
## Datums und Zeitfelder
Eine Konvertierung eines Datum und einer Uhrzeit in den Typ `java.util.Date` liefert mit folgender Anweisung falsche Werte (Sie sind um den Zeitzonenoffset gegenüber UTC verschoben):
```java
Date abapDate = jcoRecord.getDate(1);
Date abapTime = jcoRecord.getDate(2);

System.out.println(new Date(abapDate.getTime() + abapTime.getTime()));
```
Daher muss noch der Zeitzonenoffset mit in die Berechnung einbezogen werden:
```java
Date abapDate = jcoRecord.getDate(1);
Date abapTime = jcoRecord.getDate(2);

Calendar calendar = Calendar.getInstance();
calendar.setTime(abapTime);
System.out.println(new Date(abapDate.getTime() + calendar.getTimeZone().getOffset(abapTime.getTime()) + abapTime.getTime()));
```

## Uhrzeiten
Bei Uhrzeiten, die in dem ABAP-Typ `TIMS` konveriert werden sollen, ist bei Java oder in C# darauf zu achten, dass die Stunde im 24-Stunden nicht im 12 Stunden Format angegeben werden muss. Folgende Listings konvertieren die Uhrzeit falsch.
```java
new SimpleDateFormat("hhmmss").format(new Date());
```

```C#
DateTime.Now.ToString("hhmmss")
```

Statt ein kleines `h` muss ein großes `H` angegeben sein.
```java
new SimpleDateFormat("HHmmss").format(new Date());
```

```C#
DateTime.Now.ToString("HHmmss")
```

## JCo Auswahl der richtigen Installation-Bibliothek
Es gibt zwei Pakete:
- sapjco3-ntintel-3.1.8.zip for a 32-bit JRE running on a 32- or 64-bit AMD or INTEL x86 processor
- sapjco3-ntamd64-3.1.8.zip for a 64-bit JRE running on a 64-bit AMD or INTEL x86 processor
Welche JRE-Version (32-bit oder 64-bit) lässt sich mit dem Befehl `java -version` herausfinden.

## JCO Server Name
Für den Server-Name, der beim Aufruf der Methode `JCoServerFactory.getServer(String name)` Verwendung findet, wird die Datei mit dem Servernamen und der Endung `jcoServer` gelesen, die die Serverparameter enthält. Serverparameter sind im Interface `com.sap.conn.jco.ext.ServerDataProvider` aufgelistet.
Die Datei sollte im Arbeitsverzeichnis liegen.

## JCo Server Destinationen
Die JCo Server Destination (Parameter `jco.server.repository_destination`) verweist auf eine Datei mit dem Destinationsnamen und der Endung `jcoDestination` (Destinationsdatei). Die Destinationsdatei enthält die Anmeldedaten des AS ABAP. Fehlt die Destinationsdatei, wird folgende Fehlermeldung ausgegeben:
```
Exception in thread "main" com.sap.conn.jco.JCoException: (106) JCO_ERROR_RESOURCE: Server defaul
t repository destination ... is invalid: Destination ... does not exist
        at com.sap.conn.jco.rt.AbstractServer.update(AbstractServer.java:240)
        at com.sap.conn.jco.rt.CPICServer.<init>(CPICServer.java:59)
        at com.sap.conn.jco.rt.CPICServerManager$CPICServerFactory.createServer(CPICServerManager
.java:302)
        at com.sap.conn.jco.rt.CPICServerManager$CPICServerFactory.createServer(CPICServerManager
.java:298)
        at com.sap.conn.jco.rt.AbstractServerManager.getServerForType(AbstractServerManager.java:
55)
        at com.sap.conn.jco.rt.GenericServerManager.getServerForType(GenericServerManager.java:57
)
        at com.sap.conn.jco.rt.GenericServerManager.getServer(GenericServerManager.java:123)
        at com.sap.conn.jco.rt.StandaloneServerFactory.createNewServer(StandaloneServerFactory.ja
va:616)
        at com.sap.conn.jco.rt.StandaloneServerFactory.update(StandaloneServerFactory.java:487)
        at com.sap.conn.jco.rt.StandaloneServerFactory.getServerInstance(StandaloneServerFactory.
java:215)
        at com.sap.conn.jco.server.JCoServerFactory.getServer(JCoServerFactory.java:85)
        at com.oetinger.waageserver.RfcServer.<init>(RfcServer.java:42)
        at com.oetinger.waageserver.ConsoleProxy.main(ConsoleProxy.java:6)
Caused by: com.sap.conn.jco.JCoException: (106) JCO_ERROR_RESOURCE: Destination ... does not exist
        at com.sap.conn.jco.rt.DefaultDestinationManager.update(DefaultDestinationManager.java:22
7)
        at com.sap.conn.jco.rt.DefaultDestinationManager.searchDestination(DefaultDestinationMana
ger.java:463)
        at com.sap.conn.jco.rt.DefaultDestinationManager.getDestinationInstance(DefaultDestinatio
nManager.java:112)
        at com.sap.conn.jco.JCoDestinationManager.getDestination(JCoDestinationManager.java:56)
        at com.sap.conn.jco.rt.AbstractServer.update(AbstractServer.java:217)
        ... 12 more
```
Die Destinationsdatei sollte im Arbeitsverzeichnis liegen.
