# Code-Inspector
## Token
Kommentare werden als ein Token gesehen, daher wenn z.B. wenn Kommentare mit
`@TODO` gesucht werden soll: `*@TODO*` verwenden..
Literale in Hochkommata setzen, sonst werden diese nicht gefunden!

## Objektmengen
- Werden einzelne Programme angegeben, werden die Includes mit durchsucht. Allerdings werden generierte Objekte bisher nicht ber�cksichtigt (SAP-Query usw.)! Diese haben keinen Objektkatalogeintrag.
- Nur Eigenentwicklungen und Erweiterungen (FUGS X...) werden in der Objektmenge ber�cksichtigt. 
- Web-Dynpro Anwendungen werden vollst�ndig durchsucht!
- Pakete werden nicht rekursiv durchsucht.

## Syntaxpr�fung
Syntaxfehler in Dynpros werden nicht gepr�ft

# Verwendungsnachweis
## Badi
Es gibt eine Badi-Definition und eine Erweiterungsimplementierung.
Ausgehend von der Erweiterungsimplementierung liefert der Verwendungsnachweis keine Ergebnisse. 
Verwendungsnachweis muss von der Badi-Definition ausgehen.
## Append-Strukturen
Werden nicht im Verwendungsnachweis gefunden!


