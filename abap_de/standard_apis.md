# Umgebung
- SAP Basis 7.40

# Kurztexte (Material-, Warengruppenkurztexte)

Diese werden nur für flache Dictionary-Strukturen, nicht für tiefe Strukturen gelesen:
```abap
DATA(text_identifier) = CAST if_text_identifier( NEW cl_text_identifier( ) ).
text_identifier->read_text( EXPORTING tabname = 'FLACHE_STRUKTUR'
  fieldnames = fieldnames
  record = record
  record_specified = abap_true
  language = sy-langu
  IMPORTING
  text_identifier_results = DATA(text_identifier_results)
  EXCEPTIONS
  illegal_table = 3.
```
Die Ausnahme `illegal_table`wird für tiefe Strukturen ausgelöst.

# Materialstamm
Alle Programme und Funktionsbausteine, die `MATERIAL_MAINTAIN_DARK` verwenden (Programm `RMDATIND`, Funktionsbaustein `BAPI_MATERIAL_SAVEDATA`) erstellen die folgenden Sichten nicht:
- Dokumentdaten
- Prognoseausführung
- durchschnittlicher Werksbestand
- geplante Änderungen
- Fertigungsversionen
- Klassifizierung
- Kuppelproduktion
- Mengeneinheitengruppe
- QM-Prüfdaten
- Revisionsstand
- Verkaufspreis
- Vorlagematerial
- Zollpräferenzen und Ausfuhrgenehmigungen
- Zuordnung eines konfigurierbaren Materials einschließlich dessen Varianten
(siehe Dokumentation `MATERIAL_MAINTAIN_DARK`)

# Klassifizierungs-API

## `BAPI_OBJCL_CHANGE` und `BAPI_GOODSMVT_CREATE`
In einer SAP-LUW wird die Klassifizierung nicht geändert. 
```abap
CALL FUNCTION 'BAPI_OBJCL_CHANGE'
  EXPORTING
    objectkey = CONV objnum( |{ batch-matnr }{ batch-werks }{ batch-charg }| )
    objecttable = 'MCHA'
    classnum = '022'
    classtype = 'CHARGE'
  TABLES
    allocvaluesnumnew = numeric_values
    allocvaluescharnew = char_values
    allocvaluescurrnew = currency_values
    return = messages.

DATA(goodsmvt_items) = VALUE tab_bapi_goodsmvt_item(
  ( material = batch-matnr plant = batch-werks stge_loc = batch-lgort batch = batch-charg
    entry_qnt = quantity
    move_mat = target_batch-matnr move_plant = target_batch-werks move_batch = target_batch-charg
    move_stloc = target_batch-lgort ) ).

CALL FUNCTION 'BAPI_GOODSMVT_CREATE'
  EXPORTING
    goodsmvt_header = goodsmvt_header
    goodsmvt_code = VALUE #( gm_code = '03' )
  TABLES
    goodsmvt_item = goodsmvt_items.
```
Es muss ein `COMMIT WORK` nach `BAPI_OBJCL_CHANGE` erfolgen, damit die Klassifizierung geändert werden kann:
```abap
CALL FUNCTION 'BAPI_OBJCL_CHANGE'.
COMMIT WORK.
CALL FUNCTION 'BAPI_GOODSMVT_CREATE'.
```

## Intervalwerte
```abap
CALL FUNCTION 'BAPI_OBJCL_CHANGE'
  TABLES
    allocvaluesnummnew = new_properties.
```
Bei Intervalwerten muss `value_relation = '3'` in `new_properties` gesetzt werden. Ohne diese Angabe wird nur die Untergrenze gesichert.

## Datumsmerkmale
Datumsmerkmale sind unter den numerischen Merkmalen zu finden.

## `CLAF_CLASSIFICATION_OF_OBJECTS`
```abap
CALL FUNCTION 'CLAF_CLASSIFICATION_OF_OBJECTS'
  EXPORTING
    initial_charact = abap_false
    object = object
    classtype = classtype
    objecttable = objecttable
  TABLES
    i_sel_characteristic = sel_characteristics
    t_objectdata = classification.
```
Wenn `sel_characteristics` gefüllt ist, enthält der Aktualparameter `classification` alle Parameter aus `sel_charateristics` auch diese mit Initialwert.

# Buchung Materialbelege mit `BAPI_GOODSMVT_CREATE`

- Schlägt die Buchung fehl und es wird kein `ROLLBACK WORK` ausgelöst, bleiben die SAP-Sperren bestehen.
- Das BADI `MB_BAPI_GOODSMVT_CREATE` ist für die Verarbeitung der Erweiterungen im Parameter `EXTENSIONIN`zuständig. Das BADI benötigt eine Kundenimplementierung, um Append-Strukturen der Tabelle `MSEG` zu füllen. 
- In einer SAP-LUW kann `BAPI_GOODSMVT_CREATE` nur einmal ausgeführt werden.
- Es können nur positive Mengen angegeben werden. Negative Mengen führen zum Fehler 192(M7) `Feld IMSEG-ERFMG ist negativ. Es können nur positive Werte verarbeitet werden`.

## Customer-Exits Materialbelege
Im Customer-Exit `EXIT_SAPMM07M_004` können einzelne Klassifizierungsmerkmale bei Buchung eines Materialbeleges gesetzt werden. Ungültige Klassifizierungsmerkmale haben keinen Fehler zur Folge, anstattdessen bleibt das Merkmal leer.

## Buchen in Qualitätsbestand (Wareneingang Prozessaufträge)
Wird im Prozessauftrag Qualitätsbestand angegeben und die Buchung mittels `BAPI_GOODSMVT_CREATE` durchgeführt, erfolgt die Buchung trotzdem in den freien Bestand. Das BAPI ignoriert die Einstellung aus dem Prozessauftrag.
```abap

PARAMETERS: aufnr TYPE aufnr.

START-OF-SELECTION.
  DATA:
    messages TYPE bapiret2_t.

  DATA(goodsmvt_header) = VALUE bapi2017_gm_head_01(
    pstng_date = sy-datum doc_date = sy-datum ).
  DATA(goodsmvt_items) = VALUE bapi2017_gm_item_create_t(
   ( orderid = aufnr entry_qnt = 1
     move_type = '101' mvt_ind = 'F' ) ).

  CALL FUNCTION 'BAPI_GOODSMVT_CREATE'
    EXPORTING
      goodsmvt_header  = goodsmvt_header
      goodsmvt_code    = VALUE bapi2017_gm_code( gm_code = '02' )
    TABLES
      goodsmvt_item    = goodsmvt_items
      return           = messages.
  IF line_exists( messages[ type = 'E' ] ).
    ROLLBACK WORK.
  ELSE.
    COMMIT WORK.
  ENDIF.
  cl_demo_output=>display( messages ).
```

# Lieferscheine
Lieferscheine lassen sich mit dem Funktionsbaustein `WS_DELIVERY_UPDATE_2` bearbeiten. Der Lagerort lässt sich nicht zusammen mit der Kommissionierung ändern. Stattdessen muss im ersten Funktionsaufruf der Lagerort geändert werden und im zweiten Aufruf die Kommissionierung.
```abap
METHOD change_storage_location.
  DATA:
    messages TYPE tab_prott.

  DATA(header) = VALUE vbkok( vbeln_vl = delivery_note-vbeln ).
  DATA(items) = VALUE tt_vbpok(
    ( vbeln_vl = delivery_note-vbeln posnr_vl = delivery_note-posnr
      vbeln = delivery_note-vbeln posnn = delivery_note-posnr
      lfimg = delivery_note-quantity matnr = delivery_note-material_no lgort = delivery_note-storage_loc_no
      kzlgo = abap_true xwmpp = abap_true ) ).

  CALL FUNCTION 'WS_DELIVERY_UPDATE_2'
    EXPORTING
      vbkok_wa  = header
      commit    = abap_false
      delivery  = delivery_note-vbeln
     TABLES
       vbpok_tab = items
       prot      = messages.
  " error handling

ENDMETHOD.

METHOD update_picking.

   DATA(header) = VALUE vbkok( vbeln_vl = delivery_note-vbeln_lif ).
   DATA(items) = VALUE tt_vbpok(
     ( vbeln_vl = delivery_note-vbeln posnr_vl = delivery_note-posnr
       vbeln = delivery_note-vbeln posnn = delivery_note-posnr
       lfimg = delivery_note-quantity matnr = delivery_note-material_no charg = delivery_note-batch_no
       lgort = delivery_note-storage_loc_no taqui = abap_true ) ).

   CALL FUNCTION 'WS_DELIVERY_UPDATE_2'
     EXPORTING
       vbkok_wa       = header
       commit         = abap_false
       delivery       = delivery_note-vbeln
       update_picking = abap_true
     TABLES
       vbpok_tab      = items
       prot           = messages.
  " error handling
ENDMETHOD.

METHOD change_delivery_note

  change_storage_location( delivery_note ).
  COMMIT WORK AND WAIT.
  update_picking( delivery_note ).
  COMMIT WORK AND WAIT.

ENDMETHOD.
```

Die Export- bzw. Außenhandelsdaten können nicht mit dem Funktionsbaustein `WS_DELIVERY_UPDATE_2` geändert werden. Stattdessen stehen im Customizing die Tabellen `T605Z` und `T616Z` zur Verfügung, um die Geschäftsart und das Exportverfahren vorzubelegen.

# Bestellungen erstellen mit `BAPI_PO_CREATE1`
## Preisfindung
Erste Möglichkeit der Bestellposition einen Preis mitzugeben, ist die Kondition wie in [Hinweis 2115641](https://me.sap.com/notes/2115641) und im folgenden Listing beschrieben, dem Funktionsbaustein `BAPI_PO_CREATE1` mitzugeben.
```abap
DATA(conditions) = VALUE bapimepocond_tp(
  ( itm_number = 10 cond_type = 'PB00' cond_value = 200 change_id = 'U' ) ).
DATA(conditionsx) = VALUE bapimepocondx_tp(
  ( itm_number = 10 cond_type = abap_true cond_value = abap_true change_id = abap_true ) ).
```
Zweite Möglichkeit ist der Einkaufsinfosatz. Wenn eine Preiskondition im Einkaufsinfosatz vorhanden ist, wird diese verwendet.
Die dritte Möglichkeit ist die letzte Bestellung, aus der der Preis übernommen wird, wenn die erste und zweite Möglichkeit nicht greift. Die Preissuche in der letzten Bestellung lässt sich mit dem Parameter `NO_PRICE_FROM_PO` deaktivieren. Bei Deaktivierung (`NO_PRICE_FROM_PO = abap_true`) wird der Nettopreis aus dem Parameter `POITEM` verwendet.

## Positionsnummern
Die Positionsnummern aus dem Parameter `POITEM` werden nur verwendet, wenn dem Strukturfeld `POHEADER-ITEM_INTVL` dem Wert `abap_true` mitgegeben wird. Sonst zählt der Funktionsbaustein `BAPI_PO_CREATE1` die Positionsnummern anhand des Positionsnummerninterval hoch [siehe Hinweis 418683](https://me.sap.com/notes/418683).

# Bestellungen ändern mit `BAPI_PO_CHANGE`
Bei Änderung einer Position, deren Wert nach der Änderung kleiner als die kleinste Währungseinheit ist (z.B. 0,01 EUR oder 0,01 USD) kommt es zur Fehlermeldung `Bitte Nettopreis eingeben` (Nachrichtenklasse 06 Meldung 215).

# Faktura mit `BAPI_BILLINGDOC_CREATEMULTIPLE`
Wenn keine Kondition für Preis `PR00` gepflegt ist in den Konditionstabellen, übernimmt der Funktionsbaustein den eingebenen Preis im Parameter `CONDITIONSDATAIN` nicht.

# OFFICEINTEGRATION

## Windows
Eine bestehende Datei wird nicht in Ihrem Pfad geöffnet, sondern es wird eine Kopie im Pfad `c:\Users\<user>\AppData\Local\Temp` erstellt und diese geöffnet!
```abap
c_oi_container_control_creator=>get_container_control(
  IMPORTING control = DATA(control)
    error   = DATA(error) ).
raise_oi_error( error ).

control->init_control( EXPORTING
  inplace_enabled     = abap_true
  no_flush            = abap_true
  r3_application_name = file_title
  parent              = cl_gui_container=>screen0
  IMPORTING  error   = error ).
raise_oi_error( error ).

control->get_document_proxy( EXPORTING document_type  = 'Excel.Sheet'
  IMPORTING document_proxy = DATA(document_proxy)
    error = error ).
raise_oi_error( error ).

document_proxy->open_document( EXPORTING
  document_url = |file://{ file_path }|
  IMPORTING error = error ).
raise_oi_error( error ).
```

## Fehlerbehandlung
Der Exportparameter `error` ist instanziiert auch wenn die Operation erfolgreich verlaufen ist!
Wenn die Operation nicht erfolgreich verlaufen ist, steht das Attribut `has_failed` auf ABAP_TRUE.
```abap
METHOD raise_oi_error.
    DATA: message TYPE syst.

    IF error IS BOUND AND error->has_failed = abap_true.
      error->get_message( IMPORTING message_id = message-msgid
        message_number = message-msgno param1 = message-msgv1
        param2 = message-msgv2 param3 = message-msgv3
        param4 = message-msgv4 ).
      RAISE EXCEPTION TYPE zcx_oi_error
        EXPORTING
          syst_at_raise = message.
    ENDIF.

  ENDMETHOD.
```

# OLE
## Asynchrones Verhalten
Die Aufrufe erfolgen asynchron. Bsp.:
```abap
CREATE OBJECT excel_ole2_object 'Excel.Application'.
SET PROPERTY OF excel_ole2_object 'Visible' = 1.
GET PROPERTY OF excel_ole2_object 'Workbooks' = book.
CALL METHOD OF book  'Open'
  EXPORTING #1 = file_path.
CALL METHOD OF excel_ole2_object 'Quit'
```
Die 'Quit'-Anweisung wartet nicht bis das Programm geschlossen wurde (insbesondere wenn noch ein Popup kommt, ob die Datei gesichert werden soll).

## `KCD_EXCEL_OLE_TO_INT_CONVERT`
Beim Lesen einer Excel-Datei mit dem Funktionsbaustein `KCD_EXCEL_OLE_TO_INT_CONVERT` enthält die Rückgabetabelle `intern` nur die gefüllte Felder. Leere Felder erhalten keinen Eintrag in dieser Tabelle.
```abap
CALL FUNCTION 'KCD_EXCEL_OLE_TO_INT_CONVERT'
  EXPORTING
    filename = file_name
    i_begin_col = header-begin_column
    i_begin_row = header-begin_row
    i_end_col = header-end_column
    i_end_row = header-end_row
  TABLES
    intern = fields
  EXCEPTIONS
    inconsistent_parameters = 4
    upload_ole = 6.
```
### Numerische und Datums-Werte
Diese werden unverkonvertiert übernommen (z.B. 1,44 wird als 1,44 übernommen und nicht in der internen Darstellung 1.44 und 20.11.2021 als 20.11.2021 und nicht als 20211120).
### Zeilen- und Spaltennummer
Die Zeilen- und Spaltennummern sind relativ zur Startzeile bzw. Startspalte.

## Outlook 365
Outlook 365 in 64-bit Version kann in Zusammenhang mit SAPGUI 7.40 (Version 7400.3.8.1123) Probleme bei Steuerung über OLE verursachen.
Workaround um E-Mail mit Outlook zu versenden:
```abap
cl_gui_frontend_services=>execute( EXPORTING
  application = 'Outlook'
  parameter = |/m "mailto:" /a "{ path }"|
  EXCEPTIONS
   OTHERS = 4 ).
```

# Dateisystempfade
`cl_fs_path=>create_smart_path( name = '/etc/a{ddd.txt' force_absolute = abap_true )`
wirft eine Ausnahme, da der Pfadname das Sonderzeichen `{` enthält. Die Sonderzeichen `{` und `}` sind Template-Literale, die ersetzt werden.

# Verzeichnisse auslesen mit Funktionsbaustein `EPS_GET_DIRECTORY_LISTING`
Der Parameter `FILE_MASK` unterstützt unter Windows keine Glob-Pattern wie `*.csv`
```abap
CALL FUNCTION 'EPS_GET_DIRECTORY_LISTING'
  EXPORTING
    dirname = dir_name
    file_mask = '*.csv'
  TABLES
    dir_list = directory_entries.
* directory_entries ist leer:
cl_demo_output=>display( directory_entries ).
```

Bei UNC-Pfaden (z. B. `\\server\c$\data`), die auf nicht freigegebene Ressourcen verweisen, wird die Ausnahme `READ_DIRECTORY_FAILED` ausgelöst. Grund sind fehlende Berechtigungen für UNC-Pfade.

Der Parameter `DIR_LIST` enthält keine Unterverzeichnisse.

# ALPHA-Konvertierung
Wenn `strlen( Input-Parameter )` > definierte Länge Output-Parameter führt dies zum Laufzeitfehler `CONV_EXIT_FIELD_TOO_SHORT`.
```abap
DATA: input(6) TYPE c VALUE '123456',
      output(4) TYPE c.

CALL FUNCTION 'CONVERSION_EXIT_ALPHA_INPUT'
  EXPORTING
    input = input
  IMPORTING
    output = output.
```

# Signatur Funktionsbaustein
## `RFC_GET_FUNCTION_INTERFACE_P`
Namen der Parameterbezugstypen können länger als 30 Zeichen sein (z.B. ein Typ aus einer globalen Klasse). Der Funktionsbaustein `RFC_GET_FUNCTION_INTERFACE_P` schneidet diese Namen nach 30 Zeichen ab und mit dem abgeschnittenen Namen lässt sich keine Datenreferenz erzeugen. 

# HTTP-Clients (Klasse `cl_http_client`)
## TLS-Versionen
- TLS1.2 wird erst mit SAP Common Crypto Lib Version 8.4.31 supported. Die Version ist in strust `Umfeld -> SSF Version anzeigen` ersichtlich. Ebenfalls muss der Parameter `ssl/client_ciphersuites` richtig konfiguiert sein (Hinweis 510007), damit TLS1.2-Unterstützung funktioniert.

## Server Name Indication
Subdomains können auf die gleiche IP-Adresse verweisen, aber unterschiedliche Zertifikate zurückgeben. Beispiele sind `https://api.deepl.com` und `https://api-free.deepl.com`. Die Klasse `cl_http_client` verwendet dabei nur SNI (Server Name Indication), um den Servernamen im SSL-Handshake mitzugeben, wenn SNI im Profilparameter `icm/HTTPS/client_sni_enabled` aktiviert ist. Ohne SNI wird der SSL-Handshake eventuell nicht mit dem richtigen Zertifikat durchgeführt und schlägt daher mit Code `SSSLERR_SERVER_CERT_MISMATCH` fehl.

## Query-Parameter
Duplikative Parameter mit demselben Namen werden aus der Request-URI entfernt. Nur der letzte Parameter wird der Request-URI übergeben.

## Verbindungen
Verbindungen sollten mit der Methode `close` geschlossen werden wie im folgenden Listing. Sonst kommt es bei einer großen Anzahl von Clientverbindungen zur Fehlermeldung `HTTP_NO_MEMORY`.
```abap
cl_http_client=>create_by_url(
  EXPORTING
    url = 'https://gitlab.com'
  IMPORTING
    client = DATA(http_client) ).

" get data
" ...
" close
http_client->close( ).
```

# HTTP-Server
## Content-Length
Wird der `Content-Length`-Header in der HTTP-Anwort explizit gesetzt und der Header ist kleiner als der Response-Body, wird der Response-Body abgeschnitten.
```abap
METHOD if_http_extension~handle_request.

  server->response->if_http_entity~set_header_field( name = 'content-type' value = 'text/plain;charset=utf-8' ).
  server->response->if_http_entity~set_header_field( name = 'content-length' value = '2' ).
  server->response->if_http_entity~set_cdata( 'ABC' ).

ENDMETHOD.
```
Ein HTTP-Aufruf übergibt nur `AB` im Response-Body.
```
HTTP/1.0 200 OK
content-type: text/plain;charset=utf-8
content-length: 2

AB
```

## Single-Sign-On
Der SAP Server erstellt im Cookie `MYSAPSSO2` zeitlich begrenzte Single-Sign-On-Tickets. (Einstellungen befinden sich in den Parametern `login/create_sso2_ticket` und `login/ticket_expiration_time` - Transaktion RZ11).
Wird dem HTTP-Request ein ungültiges oder abgelaufenes Ticket und gültige Anmeldedaten mitgegeben, führt dies trotzdem zum HTTP-Statuscode `401 Unauthorized`.

# Senden von Emails
Schlägt das Senden von Emails an externe Empfänger mit der Fehlermeldung (Nachrichtenklasse XS, Nr. 855) `550 5.7.54 SMTP; Unable to relay recipient in non-accepted domain` fehl, der SMTP-Server ist ein Microsoft-Exchange-Server und in den SAPConnect-Einstellungen sind keine Authentifizierungsdaten eingegeben, ist die IP-Adresse des SAP Servers nicht in den Einstellungen der Microsoft Exchange Empfangskonnektoren vorhanden.
In diesem Modus arbeitet Microsoft Exchange Server als [anonymes externes Relay](https://www.frankysweb.de/exchange-2016-anonymes-relay-erlauben-5-7-54-unable-relay/) und der Zugriff auf das Relay kann auf bestimmte IP-Adressen eingeschränkt werden.
