# Stolperfallen in der Formularentwicklung

## SAPScript
### Barcodes
- Die Zeilenh�he sollte manuell angepasst werden (3 Zeilen oder gr��er), sonst ist der Barcode nur eine Zeile gro�.
- Es wird ein Platzhalter von 50mm Breite reserviert auch wenn das Bitmap k�rzer ist

## Smartforms
- Ohne Definition eines smartform-Style kann nicht mit SAPScript Texten gearbeitet
  werden.
  Bei Bedarf m�ssen die Stiele manuell kopiert werden.
   
  Sonst werden die Zeichenformate nicht unterst�tzt.

- Tabulatoren:
  Wenn ein Tabulator zu eng definiert wird, wandern alle restlichen Spalten
  eine Position r�ckw�rts.
  D.h. es gibt eine Verschiebung in den Spalten.

- Tabellenrahmen
  Nur wenn im Kopfbereich die Standard-Zeile steht, wird der obere Rahmen gedruckt

## Internationalisierung
### Druck �ber Spool
Ohne die Verwendung von Unicode Printing Enhancement (UPE) werden Zeichen, die nicht im Zeichensatz des Druckers enthalten sind, durch Hashmarks (`#`) ersetzt. Dies betrifft SapScript-Formulare und Smartforms. UPE ist im Hinweis [1812076](https://me.sap.com/notes/1812076) beschrieben.
Der Zeichensatz des Druckers wird im Ger�tetyp festgelegt.
![Ger�tetyp](device_type.png)

### SAPConnect
Damit keine Hashmarks (`#`) bei der Verwendung von SAPConnect und SAPScript/Smartforms angezeigt werden, muss der Ger�tetyp wie in Hinweis [785564](https://me.sap.com/notes/785564) beschrieben gepflegt sein. SAPConnect findet bei Nachrichten (Transaktion NACE) Verwendung, die mit der Kommunikationsstrategie `CS01` oder `INT` �ber E-Mail versendet werden.

## PDF-Formulare
- Eine ABAP-Tabelle aus dem Kontext kann nicht mehrfach in den Datenbindungen verwendet werden.
  Jede Tabelle kann in nur einer Bindung verwendet werden.
- Typpr�fung: Es findet keine Typpr�fung bei der Datenbindung statt. Eine Zeichenkette
  kann an ein numerisches - oder Dezimalfeld gebunden werden. Enth�lt die Zeichenkette nur eine positive Zahl
  oder ist leer, f�hrt dies zu keinem Fehler. Sobald die Zeichenkette eine negative Zahl (Vorzeichen rechts)
  oder Buchstaben enth�lt, f�hrt dies zu einem Fehler.
- Entfernen von Strukturfeldern: Dies f�hrt unter Umst�nden zu Syntaxfehlern, wenn die Struktur in der Schnittstelle Anwendung findet. Der entsprechende Knoten muss aktualisiert und das Formular neu aktiviert werden, damit der Syntaxfehler verschwindet.

### Scripting mit JavaScript
- `element.rawValue === undefined`, wenn ein gebundenes ABAP-Feld initial ist.
