# Stolperfallen bei der Arbeit mit SAPGUI-Controls

## Table controls
### PROCESS AFTER INPUT:
Event wird auch bei Scrollen geworfen. `sy-ucomm` behält dabei seinen vorherigen Wert. D. h. das letzte Eingabekommando wird beim Scrollen geworfen.
### Falsche Darstellung
Folgende Konstellation stellt die Daten im Table control falsch dar.
Dynpro-Ablauflogik:
```abap
PROCESS BEFORE OUTPUT.
  LOOP AT itab INTO wa WITH CONTROL sample_control.
  ENDLOOP.

PROCESS AFTER INPUT.
  FIELD carrid MODULE on_carrid_field_changed ON REQUEST.
  LOOP AT itab.
    MODULE transfer_wa_to_itab.
  ENDLOOP.
```

ABAP-Programm:
```abap
DATA: itab TYPE STANDARD TABLE OF sbook,
      wa TYPE sbook.
CONTROLS: sample_control TYPE TABLEVIEW USING SCREEN '0001'.

MODULE on_carrid_field_changed INPUT.
  PERFORM fill_itab.
ENDMODULE.

MODULE transfer_wa_to_itab INPUT.
  " modify-Statement sorgt fuer falsche Darstellung.
  MODIFY itab FROM wa INDEX sample_control-current_line.
ENDMODULE.
```

Das Problem lässt sich wie folgt beheben.
Dynpro-Ablauflogik:
```abap
PROCESS BEFORE OUTPUT.
  MODULE reset_changed_status.
  LOOP AT itab INTO wa WITH CONTROL sample_control.
  ENDLOOP.

PROCESS AFTER INPUT.
  FIELD carrid MODULE on_carrid_field_changed ON REQUEST.
  LOOP AT itab.
    MODULE transfer_wa_to_itab.
  ENDLOOP.
```

ABAP-Programm:
```abap
DATA: itab TYPE STANDARD TABLE OF sbook,
      wa TYPE sbook,
      selection_changed TYPE abap_bool.
CONTROLS: sample_control TYPE TABLEVIEW USING SCREEN '0001'.

MODULE reset_changed_status OUTPUT.
  selection_changed = abap_false.
ENDMODULE.

MODULE on_carrid_field_changed INPUT.
  selection_changed = abap_true.
  PERFORM fill_itab.
ENDMODULE.

MODULE transfer_wa_to_itab INPUT.
  IF selection_changed = abap_false.
    MODIFY itab FROM wa INDEX sample_control-current_line.
  ENDIF.
ENDMODULE.
```

### Änderungen der zugrundeliegende Tabelle (Zeilenumfang)
`REFRESH CONTROL` ist immer zu rufen, damit das Table Control die Anzahl Zeilen korrekt anzeigt.
### Buttons
Bei Klick auf einen Button wird das Kommando für alle Zeilen ausgelöst.
Dynpro-Ablauflogik:
```abap
PROCESS BEFORE OUTPUT.
  LOOP AT itab INTO wa WITH CONTROL sample_control.
  ENDLOOP.

PROCESS AFTER INPUT.
  LOOP AT itab.
    MODULE button_pressed.
  ENDLOOP.
```

ABAP-Programm:
```abap
MODULE button_pressed INPUT.
  IF sy-ucomm = 'COMMAND_1'.
    " Ausloesung in allen Zeilen
  ENDIF.
ENDMODULE.
```
 
### Scrollen im Table control
Scrollen löst PAI aus. Wird beim Scrollen `REFRESH CONTROL` gerufen, setzt dieses Statement die Scroll-Position zurück. D. h. das Table-Control lässt sicht nicht scrollen.

### `GET CURSOR LINE DATA(line_index)`
Im Table-Control ist der Index abhängig von der Scroll-Position. Index 1 bedeutet der Cursor steht auf der ersten dargestellte Zeile, nicht auf die erste Zeile der zugrundeliegenden Tabelle.

### Radio-Buttons
Radio-Buttons werden nach Zeilen nicht nach Spalten gruppiert. Enthält eine Zeile Radio-Buttons, kann nur eine Zeile markiert sein. Werden mehrere Zeilen markiert (beispielsweise wenn Radio-Buttons in mehreren Spalten verwendet wurden), führt dies zum Laufzeitfehler `DYNP_TOO_MANY_RADIOBUTTONS_ON`.

### Zeilen löschen
Mit folgender Konstellation lässt sich nur die letzte Zeile loeschen.
Dynpro-Ablauflogik:
```abap
PROCESS BEFORE OUTPUT.
  LOOP AT itab INTO wa WITH CONTROL sample_control.
  ENDLOOP.

PROCESS AFTER INPUT.
  LOOP AT itab.
    " mark ist Markierspalte
    FIELD mark MODULE delete_rows ON INPUT.
    MODULE transfer_wa_to_itab.
  ENDLOOP.
```

ABAP-Programm:
```abap
DATA: itab TYPE STANDARD TABLE OF sbook,
      wa TYPE sbook.
CONTROLS: sample_control TYPE TABLEVIEW USING SCREEN '0001'.

MODULE delete_rows INPUT.
  IF sy-ucomm = 'DELETE_ROW'.
    DELETE itab INDEX sample_control-current_line.
  ENDIF.
ENDMODULE.

MODULE transfer_wa_to_itab INPUT.
  MODIFY itab FROM wa INDEX sample_control-current_line.
ENDMODULE.
```

### Mehrere Table-Controls
Wenn im einem Dynpro mehrere Table-Controls definiert sind, müssen zu den Zeitpunkten `PBO` und `PAI` die LOOPS genau in der Reihenfolge definiert werden wie in der Dynproelementliste.

## `cl_gui_textedit`
Der Methodenaufruf `cl_gui_cfw=>flush( )` sorgt dafür, das der Text zurückgegeben wird. Ohne den Flush gibt die Methode `get_textstream` keinen Text zurück.
```abap
DATA(text_editor) = NEW cl_gui_textedit( ).
text_editor->get_textstream( IMPORTING text = DATA(text) ).
cl_gui_cfw=>flush( ).
```
## ALV-Grid 
### Mehrfachinstanziierung
Wird das Grid mehrfach im selben Container instanziiert, übernimmt die Methode `cl_gui_alv_grid=>check_changed_data` die geänderten Zellen, die in den letzten Instanzen geändert wurden.
Richtig ist hier anstatt der Mehrfachinstanziierung die Methode `cl_gui_alv_grid=>refresh_table_display( )` zu rufen, wenn sich die zugrundeliegende Tabelle ändert.
### Überschriften
Ändern Sie nur die Überschriften und nicht die Dictionary-Struktur müssen Sie die Dictionary-Struktur erneut aktivieren, damit die neuen Überschriften Anwendung im Feldkatalog finden.
### Feldkatalog - Strukturänderungen
Damit sich der Feldkatalog aktualisiert, muss den Funktionsbausteinen `LVC_FIELDCATALOG_MERGE` oder `REUSE_ALV_FIELDCATALOG_MERGE` der Parameter `I_BYPASSING_BUFFER=abap_true` mitgegeben werden.
### Dezimalstellen
Dezimalstellen werden bei Feldkatalogen, die mit Bezug zu Dictionary-Strukturen erstellt wurden, nicht beachtet. In diesem Beispiel behalten alle Mengenfelder die Dezimalstellen aus den Dictionary-Datentypen:
```abap
  CALL FUNCTION 'LVC_FIELDCATALOG_MERGE'
    EXPORTING
      i_structure_name = 'ZPPI_PERFORMANCE'
    CHANGING
      ct_fieldcat = fieldcat.

    LOOP AT fieldcat ASSIGNING FIELD-SYMBOL(<field>)
        WHERE datatype = 'QUAN'.
      <field>-DECIMALS_O = '0'.
    ENDLOOP.

   CALL FUNCTION 'REUSE_ALV_GRID_DISPLAY_LVC'
     EXPORTING
       it_fieldcat_lvc = fieldcat
     TABLES
       t_outtab = perf.
```
### Hintergrundverarbeitung:
`REUSE_ALV_LIST_DISPLAY`, `REUSE_ALV_GRID_DISPLAY_LVC` kann in der Hintergrundverarbeitung gerufen werden.


## Temporäres Verzeichnis
Nach Aufruf `cl_gui_frontend_services=>get_temp_directory` muss ein Flush erfolgen, damit das Verzeichnis in der ABAP-Variable zur Verfügung steht.
```abap
cl_gui_frontend_services=>get_temp_directory( CHANGING temp_dir = temp_dir ).
cl_gui_cfw=>flush( ).
```

## Mehrfaches Erzeugen eines Containers zum Zeitpunkt `PROCESS BEFORE OUTPUT`
Alle Containerinstanzen bleiben bestehen und nur das erste wird angezeigt.
