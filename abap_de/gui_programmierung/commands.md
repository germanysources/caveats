# AT EXIT-COMMAND
Es findet kein Transport der Dynpro-Felder an das ABAP-Programm statt. Wird der Transport benötigt,
z.B. um nach einer Sicherheitsabfrage einen Beleg zu buchen, kann die Funktion nicht mit einem `AT EXIT-COMMAND`-Modul behandelt werden.
