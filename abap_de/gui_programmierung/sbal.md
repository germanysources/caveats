Anzeige Log-Protokoll mit Funktionsgruppe `SBAL_DISPLAY`:

1.	`CALL FUNCTION 'BAL_DSP_OUTPUT_FREE'` muss immer gerufen werden, wenn die Anzeige verworfen wird. Grund Funktionsbaustein `bal_dsp_output_set_data` erzeugt einen Subroutinenpool dynamisch. Davon können nur 36-Stück pro interner Sitzung erzeugt werden. Beim 37-mal wirft `bal_dsp_output_set_data` einen Fehler.

2.	`CALL FUNCTION `'BAL_DSP_LOG_TEXTFORM'`: Entweder `I_LOG_HANDLE` oder `IT_LOG_HANDLE` muss mitgegeben werden. Fehlen beide Parameter, wird kein Protokoll angezeigt, auch nicht die Protokolle im Hauptspeicher.
