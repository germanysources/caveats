## `POPUP_TO_CONFIRM`
`POPUP_TO_CONFIRM` ändert das Systemfeld `sy-ucomm`
Bespiel:
```abap
TYPES: _answer(1).
CONSTANTS: BEGIN OF answers,
  yes TYPE _answer VALUE '1',
  cancel TYPE _answer VALUE 'A',
END OF answers.

MODULE exit_command.
  PERFORM exit_command.
ENDMODULE.

FORM exit_command.
  DATA: answer TYPE _answer.

  " Bsp.: sy-ucomm EQ 'EXIT'

  CALL FUNCTION 'POPUP_TO_CONFIRM'
    EXPORTING
      text_question = text-001
    IMPORTING
      answer = answer.

  IF answer = answers-cancel.
    RETURN.
  ELSEIF answer = answers-yes.
    PERFORM save.
  ENDIF.

  " sy-ucomm enthaelt jetzt das Kommando vom Popup, nicht mehr das Kommando
  " zum Zeitpunkt PAI des Haupt-Dynpros.
  " Bsp. sy-ucomm <> 'EXIT'
  CASE sy-ucomm.
    WHEN 'EXIT'.
      LEAVE PROGRAM.
  ENDCASE.

ENDFORM.
```
