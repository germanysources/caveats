# Stolperfallen in der GUI-Programmierung Web-Dynpro

- Beim �ffnen eines Popups wird die Programmausf�hrung nicht unterbrochen (Asynchron). Daher muss eine R�ckgabe an das aufrufende Dynpro �ber ein Event geschehen.

- Ein Laufzeitfehler vom Typ `OBJECTS_OBJREF_NOT_ASSIGNED` wird erzeugt, wenn ein Context-Knoten gel�scht wird:
```abap
DATA(node) = wd_context->get_child_node( 'TABLE' ).
node->remove_element( node->get_element( ) ).
```
