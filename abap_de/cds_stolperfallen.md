# CDS-Funktion `unit_conversion`

Unter Umständen wird die gesamte Datenquelle auf der Datenbank durchlaufen.
Dies hat die Ausnahme `cx_sy_open_sql_db` zur Folge, wenn `error_handling => 'FAIL_ON_ERROR'`
gesetzt ist und Einheiten nicht ineinander umgerechnet werden können
auch wenn diese Datensätze nicht im Selektionsergebnis vorhanden sind.

# UNION-SELECT
Ohne den Zusatz ALL werden duplikative Einträge entfernt. Was sind duplikative Einträge?
todo 

# Performance
Sehr inperformant ist bei großen Tabellen die Verwendung eines Feldes, das sich aus einer Fallunterscheidung zusammensetzt, in WHERE-Klauseln oder in JOIN-Ausdrücken.
Bsp.:
```
define view zflights as select from slfight{

  key carrid,
  key connid,
  key fldate,
  case when seatsocc < seatsmax then '1' else '2' end as status

}
```

Aufruf mit Open SQL:
```ABAP
SELECT * FROM zflights WHERE status = '2';
```
