# Stoplerfallen bei der Arbeit mit Dockerfile
## COPY
Der `*`-Selektor kopiert nur Dateien, keine Unterverzeichnisse.
```
COPY dir/* /etc/dir
```
Der COPY-Befehl kopiert die Elemente innerhalb des Quellverzeichnisses in das angegebene Zielverzeichnis. Es wird im Gegensatz zum Bash `cp`-Befehl kein Verzeichnis mit demselben Namen unterhalb des Zielverzeichnisses erstellt. Im folgenden Listing kopiert der COPY-Befehl alle Elemente innerhalb von `etc/lighttpd` in das Verzeichnis `/etc`
```
COPY etc/lighttpd /etc
```

## CRON-Jobs unter Ubuntu
### Protokoll
Fehler bei cron-Jobs werden protokolliert, wenn `rsyslog` installiert und gestart ist.
```Dockerfile
RUN apt-get update && \
    apt-get install -y rsyslog
```
`entrypoint.sh`:
```sh
#!/usr/bin/env sh
service rsyslog start
cron -f
```

### Betrieb unter openSUSE
Unter openSUSE Tumbleweed (Host-OS) kommt es zu einem Berechtigungsproblem, wenn cron-Jobs in einem Ubuntu-Docker-Container laufen:
```
CRON[75]: pam_unix(cron:session): session opened for user root by (uid=0)
CRON[79]: pam_loginuid(cron:session): Error writing /proc/self/loginuid: Operation not permitted
CRON[79]: pam_loginuid(cron:session): set_loginuid failed
CRON[79]: pam_env(cron:session): Unable to open env file: /etc/default/locale: No such file or directory
```
Bei Ubuntu als Host-OS tritt dieses Problem nicht auf.

