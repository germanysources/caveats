# Release Checkliste
Die Release-Checkliste enthält alle notwendigen Schritte, um die Anwendung von den Test-Betrieb in den Produktivbetrieb zu übergeben.

- Notwendige Migrationen
- Alle Einstellungen/Konfigurationen für den Produktivbetrieb vorhanden?
- Einrichten von Batch-Jobs/Cron-Jobs
- Berechtigungen und Benutzer (Datenbankbenutzer usw.) eingerichtet?
- Webservices: SSL-Zertifikate für Produktivbetrieb vorhanden und der Webservice richtig konfiguriert, damit er die SSL-Zertifikate des Produktivbetriebs verwendet

## Container-Anwendungen
- Volumes für Datenbanken erstellt
- Umgebungsvariablen an den Produktivbetrieb angepasst?

### Docker-Rootless-Modus
Bei der manuellen Kopie von Dateien oder Verzeichnisse in das Volume muss der Eigentümer (Gruppe und der Benutzer) angepasst werden. Sonst fehlt Docker die Berechtigung, auf die Dateien zuzugreifen:
```sh
cd /var/lib/docker/volumes/$UID:$GID/volumes/$VOLUME_NAME
chown -R $UID _data
chgrp -R $GID _data
```

## SQL-Anwendungen (außer SAP-Systeme)
- Datenbankschema aktualisieren, wenn notwendig

## ABAP-Anwendungen
- Alle Transport-Aufträge in richtiger Reihenfolge transportiert?
- Berechtigungsrollen angepasst/erstellt und Nutzern zugewiesen?
- Customizing transportiert/vorhanden und Stammdaten angepasst?
- Webservices aktiviert?
