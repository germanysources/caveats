# Stolperfallen in Akzeptanztests
Akzeptanz-Test müssen möglichst nahe an die Bedingungen des Produktivbetriebes herankommen. Im Produktivbetrieb kann es dabei eine große Menge unterschiedlicher Konstellationen geben.

## ABAP/SAP Systeme
- Verwendung identischer Customizing-Einstellungen/Stammdaten, soweit möglich
- Transaktionen in selber Reihenfolge auslösen wie im Produktivsystem
- Prüfung geänderter/neuer Customizing-Einstellungen
- Alle abhängigen Programme/Transaktionen getestet?
- Texte in alle Sprachen übersetzt?

### Nach Upgrade (Support-Packages) besonders zu beachten
- Modifikationen, die an SAP-Customizing-Tabellen vorgenommen wurden, sind noch enthalten (Bsp. Textarten für Bestellanforderungen)
- Peripherie (Drucker, RFC- und Web-Anwendungen)
- RFC-Anwendungen, die mittels `RFC_READ_TABLE` Daten lesen, verwenden die richtigen Feldlängen für die Selektionsoptionen von zeichenartigen Feldern (Parameter `OPTIONS`), sonst kommt es zu Laufzeitfehlern vom Typ `SAPSQL_DATA_LOSS`
- Eigenentwicklungen: Funktionen, die von SAP für andere Anwendungszwecke vorgesehen wurden (Bsp. `BAPI_ALM_ORDER_GET_DETAIL`, um Prozessaufträge zu lesen)

## APIs (REST/GraphQL) von Drittanbietern
- Prüfung von Authentifizierungsschlüsseln (API-Keys), Zertifikate, IDs usw., ob diese noch auf das Test-/Sandboxsystem verweisen und korrekt sind
- Jobs zum Aktualisieren von OAUTH-Tokens oder Zertifikaten alle eingeplant?
- Paginierung einzelner Requests beachten. Im Testsystem sollten genügend Entitäten vorhanden sein, sodass sie nicht alle auf eine Seite (Page) passen
- Anmelden mit unterschiedlichen Benutzern, um zu prüfen, ob Nutzern wie bei der [Facebook GraphQL API](https://developers.facebook.com/docs/graph-api/overview/access-levels/) vorher eine Rolle zugewiesen werden muss.

## Web-Anwendungen
- Testen in unterschiedlichen Browsern (auch mit älteren Versionen)
- Netzwerkunterbrechungen simulieren
- Sperrbildschirme auf mobilen Geräten
- Benutzernamen in Gross- und Kleinschreibung eingeben, um zu prüfen, dass Benutzernamen nicht case sensitive sind.
- Dateiupload: Testen mit Dateien, die die Größenbeschränkungen überschreiten. Erwartet wird eine Fehlermeldung

## Container
- Test-Container möglichst identisch zu Produktivcontainer aufbauen
  (Verwendung von identischen Hostnamen, Versionen usw.)

## UI-Anwendungen
UI-Aktionen, die das Datenmodell ändern (Programmvariablen und persistentes Datenmodell in einer Datenbank) und das Datenmodell mit der Anzeige verknüpft ist, sollten auf Konsistenz geprüft werden. Datenmodell meint in diesem Kontext die Laufzeitvariablen. D.h. nach einer Aktion, die das persistente Datenmodell ändert, müssen die Programmvariablen identisch zum persistenten Datenmodell sein.
Bsp.:
Beim Starten der Applikation wird ein Verkaufsauftrag mit Stammdaten (Kunde, Artikel) gelesen. Die Applikation ändert den Verkaufsauftrag ab (persistentes Datenmodell). Die Funktion übergibt nach der Änderung den Verkaufsauftrag zurück, allerdings ohne Stammdaten. Der neue Verkaufsauftrag ohne Stammdaten wird den entsprechenden Programmvariablen zugewiesen. In der UI kommt es nun zur Inkonsistenz, da die Stammdaten fehlen.
Bsp.:
Die Applikation lässt Nutzern eingesetzte bzw. nicht eingesetzte Behälter mit Material einem Fertigungsauftrag zuweisen. Nach Speicherung eines Behälters in der Datenbank muss auch die Programmvariable, die den Behälter darstellt, aktualisiert werden.

Beim Test fallen diese Inkonsistenzen meistens auf, wenn Änderungen mehrfach durchgeführt werden, ohne die Daten neu aus dem persistenten Datenmodell zu laden.
