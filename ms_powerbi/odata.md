# OData-Feeds
## Reverse-Proxy
Wird ein OData-Feed hinter einem Reverse-Proxy aufgerufen, sollten die Pfade im durch den Reverse-Proxy-Pfad ersetzt werden. Sonst ignoriert PowerBI die Filter-Bedingung und versucht die ersten 1000 Top-Treffer zu laden.
Bsp.:
Pfad Reverse-Proxy: `/sap/proxy/ZFLIGHT_SRV`
Pfad OData-Service: `/sap/opu/odata/sap/ZFLIGHT_SRV`
Die Pfade in der Antwort müssen ersetzt werden. Original-Antwort:
```xml
<?xml version="1.0" encoding="UTF-8"?>
<app:service xml:lang="de" xmlns:sap="http://www.sap.com/Protocols/SAPData" xmlns:m="http://schemas.microsoft.com/ado/2007/08/dataservices/metadata" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:app="http://www.w3.org/2007/app" xml:base="http://host/sap/opu/odata/sap/ZFLIGHT_SRV/">
  <app:workspace>
    <atom:title type="text">Data</atom:title>
    <app:collection href="ResultSet" sap:content-version="1" sap:pageable="false" sap:deletable="false" sap:updatable="false" sap:creatable="false">
      <atom:title type="text">ResultSet</atom:title>
      <sap:member-title>Result</sap:member-title>
    </app:collection>
  </app:workspace>
  <atom:link href="http://host/sap/opu/odata/sap/ZFLIGHT_SRV/" rel="self"/>
  <atom:link href="http://host/sap/opu/odata/sap/ZFLIGHT_SRV/" rel="latest-version"/>
</app:service>
```
Nachdem Ersetzen der Pfade sollte folgende Antwort an PowerBI gesendet werden:
```xml
<app:service xml:lang="de" xmlns:sap="http://www.sap.com/Protocols/SAPData" xmlns:m="http://schemas.microsoft.com/ado/2007/08/dataservices/metadata" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:app="http://www.w3.org/2007/app" xml:base="http://host/sap/proxy/ZFLIGHT_SRV/">
  <app:workspace>
    <atom:title type="text">Data</atom:title>
    <app:collection href="ResultSet" sap:content-version="1" sap:pageable="false" sap:deletable="false" sap:updatable="false" sap:creatable="false">
      <atom:title type="text">ResultSet</atom:title>
      <sap:member-title>Result</sap:member-title>
    </app:collection>
  </app:workspace>
  <atom:link href="http://host/sap/proxy/ZFLIGHT_SRV/" rel="self"/>
  <atom:link href="http://host/sap/proxy/ZFLIGHT_SRV/" rel="latest-version"/>
</app:service>
```
