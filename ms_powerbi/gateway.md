# Stolperfallen mit dem On-Premise Gateway
## HTTP-Redirects
HTTP-Redirects werden als Datenquelle unter Umständen nicht unterstützt. Es kommt bei der Einrichtung des Gateways zu einem Anmeldefehler. Abhilfe kann ein serverseitiger Reverse-Proxy schaffen, um damit die HTTP-Redirects zum umgehen.
