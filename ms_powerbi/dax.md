# Stolperfallen bei der Arbeit mit Microsoft Power BI und DAX

## Datenaktualisierung
### Fehler `OLE DB oder ODBC: Von der Übertragsungsverbindung konnten keine Daten gelesen werden HRESULT: 0x80040E1D`
Dieser Fehler tritt bei der Aktualisierung auf und Grund dafür können falsch definierte Datentypen sein. Im Power Query-Editor sollten die Datentypen berichtigen werden.

### Fehler `The '...' column does not exist in the rowset`
Ein Grund dafür können Pivottabellen sein, wenn aus der Aufrissspalte seit der letzten Aktualisierung Werte entfernt wurden.

# Wochen
Sonntag ist der erste Tag der Woche. Die Funktion `WEEKDAY` gibt `1` für Sonntag aus. Die Wochennummer für So. den 17.09.2023 ergibt `38`. 
