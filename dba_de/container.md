# DB im Container bereitstellen
Die Verzeichnisse, die die Datenbank zur Ablage verwendet, immer als Volume mounten
## MariaDB
docker-compose.yml
```yml
version: '3'
services:

  db:
    image: mariadb
    restart: always
    volumes:
      - mariadb:/var/lib/mysql
```

## MongoDB
docker-compose.yml
```yml
version: '3'

services:

  db:
    image: mongo
    restart: always
    volumes:
      - mongo_data:/data/db
```

Wenn diese Verzeichnisse nicht als Volume gemountet sind, gehen alle Daten `docker-compose down` verloren.
