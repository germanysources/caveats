# Microsoft SQL Server (Version 2019)
Bei SQL-Servern gibt es einige Punkte zu beachten:

- Es gibt zwei Authentifizierungsmechanismen:
  - Windows Authentifizierung
  - SQL Server und Windows Authentifizierung
  Bei der Windows Authentifizierung ist eine Anmeldung mit einem SQL Server Benutzer (erzeugt mit der Anweisung `CREATE USER`) nicht möglich

- Remote Zugriff ist nur möglich, wenn der Dienst `SQL Server-Browser` aktiv ist und "TCP/IP" in der [Protokollsicht](https://stackoverflow.com/questions/13238219/sql-server-express-cannot-connect-error-28-server-doesnt-support-requested-p) aktiv ist

## Migration
### Schema
Bei der Migration müssen Sie unter Umständen noch die Identity-Eigenschaften der Spalten ergänzen, falls diese nicht vom Migrationstool kopiert wurden, beispielsweise mit den Kommandos
```sql
alter table <tabname> drop column id
alter table <tabname> add id bigint not null identity primary key
```
`id` soll dabei eine Spalte mit der Identity-Eigenschaft repräsentieren.

### Benutzer
Bei der manuellen Kopie von Nutzern auf identische Rechte achten!

## Hinzufügen als sekundäre Datenbankverbindung SAP ABAP System
### Windows Authentifizierung
Beim Hinzufügen von MSSQL-Servern kommt es unter Umständen (siehe SAP-Hinweis [2316544](https://me.sap.com/notes/0002416544)) zum Fehler "SQL-Fehler 1326 : ImpersonateWindowsUser: can't logon as DOMAIN\username" oder zum Fehler "SQL-Fehler 4060 Cannot open database \"...\" requested by the login. The login failed", wenn ein Windows-Domänenkonto bei der Anmeldung angegeben wird.
Um diesen Fehler zu vermeiden, sollte ein SQL Server Benutzer in der Anmeldung angegeben werden. Der SQL Server muss dazu in den Authentifizierungsmodus "SQL Server and Windows Authentication mode" versetzt werden.
### Engine Edition
Die DBSL Bibliothek unterstützt nur [EngineEdition](https://learn.microsoft.com/en-us/sql/t-sql/functions/serverproperty-transact-sql?view=sql-server-ver16) 3 (d.h. Enterprise Edition). SQL-Express oder andere Editionen werden nicht unterstützt.

## JDBC
### Connection URL
Connection URLs müssen wie [unter](https://learn.microsoft.com/en-us/sql/connect/jdbc/building-the-connection-url?view=sql-server-ver16) beschrieben, aufgebaut sein. Der Instanzname kann nicht wie beim SQL Server Management Studio als Pfad angehängt werden.

### Hibernate
Der Datenbankname muss als Parameter der Connection URL angegeben werden wie im folgenden Listing:
```
jdbc:sqlserver://hostname;trustServerCertificate=true;databaseName=BDE
```
Der Datenbankname kann nicht als Schemaname in den Entitäten (`@Table`-Annotation) angegeben werden. Folgendes Listing führt zu einem Validierungsfehler, da der Datenbanktreiber die Tabelle `BDE.dbo.BDE_DATA` nicht findet.
```java
@Data
@Entity
@Table( name = "BDE_DATA", schema = "BDE.dbo" )
public class CounterReading implements Serializable{

  // ...

}
```
Wird in der `@Table`-Annotation kein Schema angegeben, sollte das Standardschema `dbo` in den Benutzereinstellungen ausgewählt sein.
![Standardschema dbo](dbo_schema.png)

Sonst kommt es zu Validierungsfehlern, da der Datenbanktreiber die Tabellen nicht findet.
