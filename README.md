# Caveats and Issues
This is a collection of caveats/issues targeting the ecosystems JavaScript, TypeScript, Web-APIs, Java and ABAP. They should be useful for code review and for debugging.

## Languages
The natural language (english, german) is added as suffix to the folders.

## How the data is collected
The data was collected empirically on the ecosystems i worked on. Due to the rapid development in these ecosystems some caveats or issues may already been outdated or replaced by other caveats.
