# Stolperfallen der Arbeit mit HTML-Dokumenten in Libreoffice

## Umgebung
- LibreOffice 7.5

## Export von HTML zu PDF
- Externe Bildquellen (URLs) werden nicht unterstützt (`<img src="http://host/path.jpg">`). Die Bilder müssen lokal vorhanden sein. Bei Bedarf die Bilder daher vorher herunterladen.
- Custom-Fonts, die mit der CSS-Anweisung `@font-face` deklariert sind, ignoriert Libreoffice beim Export zu PDF. Die Schriftart muss installiert sein (Eine Kopie der TTF-Datei im `~/.fonts`- oder im `/usr/share/fonts/truetype`-Ordner genügt).

## Unoconv und Apache Tomcat
Bei der Konvertierung von Dokumenten mit unoconv kommt es zur Fehlermeldung `Unsupported URL <file:///....>: “type detection failed” .../framework/source/loadenv/loadenv.cxx` wenn die Datei nicht im Tomcat-Arbeitsverzeichnis abgelegt ist (`$CATALINE_HOME/work`).
